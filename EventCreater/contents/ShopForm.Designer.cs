﻿namespace EventCreater
{
    partial class ShopForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.shopInLabel = new System.Windows.Forms.Label();
            this.shopInTextBox = new System.Windows.Forms.TextBox();
            this.buyTextBox = new System.Windows.Forms.TextBox();
            this.selectMes = new System.Windows.Forms.Label();
            this.buyLabel = new System.Windows.Forms.Label();
            this.sellLabel = new System.Windows.Forms.Label();
            this.sellTextBox = new System.Windows.Forms.TextBox();
            this.retLabel = new System.Windows.Forms.Label();
            this.retTextBox = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.SubItemButton = new System.Windows.Forms.Button();
            this.AddItemButton = new System.Windows.Forms.Button();
            this.SellItemsLabel = new System.Windows.Forms.Label();
            this.SellItemsListBox = new System.Windows.Forms.ListBox();
            this.whenItemOverLabel = new System.Windows.Forms.Label();
            this.whenItemOverTextBox = new System.Windows.Forms.TextBox();
            this.whenNoBuyLabel = new System.Windows.Forms.Label();
            this.whenNoBuyTextBox = new System.Windows.Forms.TextBox();
            this.whenBuyLabel = new System.Windows.Forms.Label();
            this.whenBuyTextBox = new System.Windows.Forms.TextBox();
            this.selectItemBuyLabel = new System.Windows.Forms.Label();
            this.selectItemBuyTextBox = new System.Windows.Forms.TextBox();
            this.buySelectLabel = new System.Windows.Forms.Label();
            this.buySelectTextBox = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.WhenCannotSellLabel = new System.Windows.Forms.Label();
            this.whenCannotSellTextBox = new System.Windows.Forms.TextBox();
            this.WhenNoSellLabel = new System.Windows.Forms.Label();
            this.whenNoSellTextBox = new System.Windows.Forms.TextBox();
            this.WhenSellLabel = new System.Windows.Forms.Label();
            this.whenSellTextBox = new System.Windows.Forms.TextBox();
            this.selectItemSellLabel = new System.Windows.Forms.Label();
            this.selectItemSellTextBox = new System.Windows.Forms.TextBox();
            this.sellSelectLabel = new System.Windows.Forms.Label();
            this.sellSelectTextBox = new System.Windows.Forms.TextBox();
            this.shopEndTextBox = new System.Windows.Forms.TextBox();
            this.shopEndLabel = new System.Windows.Forms.Label();
            this.shopTextBox = new System.Windows.Forms.TextBox();
            this.shopLabel = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // shopInLabel
            // 
            this.shopInLabel.AutoSize = true;
            this.shopInLabel.Location = new System.Drawing.Point(33, 15);
            this.shopInLabel.Name = "shopInLabel";
            this.shopInLabel.Size = new System.Drawing.Size(86, 12);
            this.shopInLabel.TabIndex = 0;
            this.shopInLabel.Text = "入店時メッセージ";
            // 
            // shopInTextBox
            // 
            this.shopInTextBox.Location = new System.Drawing.Point(136, 12);
            this.shopInTextBox.Name = "shopInTextBox";
            this.shopInTextBox.Size = new System.Drawing.Size(345, 19);
            this.shopInTextBox.TabIndex = 1;
            this.shopInTextBox.Text = "いらっしゃいませ～";
            this.shopInTextBox.WordWrap = false;
            this.shopInTextBox.TextChanged += new System.EventHandler(this.shopInTextBox_TextChanged);
            this.shopInTextBox.Enter += new System.EventHandler(this.shopInTextBox_Enter);
            // 
            // buyTextBox
            // 
            this.buyTextBox.Location = new System.Drawing.Point(63, 113);
            this.buyTextBox.Name = "buyTextBox";
            this.buyTextBox.Size = new System.Drawing.Size(82, 19);
            this.buyTextBox.TabIndex = 8;
            this.buyTextBox.Text = "購入";
            this.buyTextBox.WordWrap = false;
            this.buyTextBox.TextChanged += new System.EventHandler(this.shopInTextBox_TextChanged);
            this.buyTextBox.Enter += new System.EventHandler(this.shopInTextBox_Enter);
            // 
            // selectMes
            // 
            this.selectMes.AutoSize = true;
            this.selectMes.Location = new System.Drawing.Point(33, 92);
            this.selectMes.Name = "selectMes";
            this.selectMes.Size = new System.Drawing.Size(150, 12);
            this.selectMes.TabIndex = 6;
            this.selectMes.Text = "選択する処理(空白で非選択)";
            // 
            // buyLabel
            // 
            this.buyLabel.AutoSize = true;
            this.buyLabel.Location = new System.Drawing.Point(33, 116);
            this.buyLabel.Name = "buyLabel";
            this.buyLabel.Size = new System.Drawing.Size(24, 12);
            this.buyLabel.TabIndex = 7;
            this.buyLabel.Text = "買う";
            // 
            // sellLabel
            // 
            this.sellLabel.AutoSize = true;
            this.sellLabel.Location = new System.Drawing.Point(199, 116);
            this.sellLabel.Name = "sellLabel";
            this.sellLabel.Size = new System.Drawing.Size(26, 12);
            this.sellLabel.TabIndex = 9;
            this.sellLabel.Text = "売る";
            // 
            // sellTextBox
            // 
            this.sellTextBox.Location = new System.Drawing.Point(229, 113);
            this.sellTextBox.Name = "sellTextBox";
            this.sellTextBox.Size = new System.Drawing.Size(82, 19);
            this.sellTextBox.TabIndex = 10;
            this.sellTextBox.Text = "売却";
            this.sellTextBox.WordWrap = false;
            this.sellTextBox.TextChanged += new System.EventHandler(this.shopInTextBox_TextChanged);
            this.sellTextBox.Enter += new System.EventHandler(this.shopInTextBox_Enter);
            // 
            // retLabel
            // 
            this.retLabel.AutoSize = true;
            this.retLabel.Location = new System.Drawing.Point(357, 116);
            this.retLabel.Name = "retLabel";
            this.retLabel.Size = new System.Drawing.Size(26, 12);
            this.retLabel.TabIndex = 11;
            this.retLabel.Text = "帰る";
            // 
            // retTextBox
            // 
            this.retTextBox.Location = new System.Drawing.Point(387, 113);
            this.retTextBox.Name = "retTextBox";
            this.retTextBox.Size = new System.Drawing.Size(82, 19);
            this.retTextBox.TabIndex = 12;
            this.retTextBox.Text = "帰る";
            this.retTextBox.WordWrap = false;
            this.retTextBox.TextChanged += new System.EventHandler(this.shopInTextBox_TextChanged);
            this.retTextBox.Enter += new System.EventHandler(this.shopInTextBox_Enter);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(24, 136);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(469, 289);
            this.tabControl1.TabIndex = 13;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tabPage1.Controls.Add(this.SubItemButton);
            this.tabPage1.Controls.Add(this.AddItemButton);
            this.tabPage1.Controls.Add(this.SellItemsLabel);
            this.tabPage1.Controls.Add(this.SellItemsListBox);
            this.tabPage1.Controls.Add(this.whenItemOverLabel);
            this.tabPage1.Controls.Add(this.whenItemOverTextBox);
            this.tabPage1.Controls.Add(this.whenNoBuyLabel);
            this.tabPage1.Controls.Add(this.whenNoBuyTextBox);
            this.tabPage1.Controls.Add(this.whenBuyLabel);
            this.tabPage1.Controls.Add(this.whenBuyTextBox);
            this.tabPage1.Controls.Add(this.selectItemBuyLabel);
            this.tabPage1.Controls.Add(this.selectItemBuyTextBox);
            this.tabPage1.Controls.Add(this.buySelectLabel);
            this.tabPage1.Controls.Add(this.buySelectTextBox);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(461, 263);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "買う";
            // 
            // SubItemButton
            // 
            this.SubItemButton.Location = new System.Drawing.Point(371, 131);
            this.SubItemButton.Name = "SubItemButton";
            this.SubItemButton.Size = new System.Drawing.Size(54, 21);
            this.SubItemButton.TabIndex = 27;
            this.SubItemButton.Text = "削除";
            this.SubItemButton.UseVisualStyleBackColor = true;
            this.SubItemButton.Click += new System.EventHandler(this.SubItemButton_Click);
            // 
            // AddItemButton
            // 
            this.AddItemButton.Location = new System.Drawing.Point(313, 131);
            this.AddItemButton.Name = "AddItemButton";
            this.AddItemButton.Size = new System.Drawing.Size(54, 21);
            this.AddItemButton.TabIndex = 26;
            this.AddItemButton.Text = "追加";
            this.AddItemButton.UseVisualStyleBackColor = true;
            this.AddItemButton.Click += new System.EventHandler(this.AddItemButton_Click);
            // 
            // SellItemsLabel
            // 
            this.SellItemsLabel.AutoSize = true;
            this.SellItemsLabel.Location = new System.Drawing.Point(25, 135);
            this.SellItemsLabel.Name = "SellItemsLabel";
            this.SellItemsLabel.Size = new System.Drawing.Size(181, 12);
            this.SellItemsLabel.TabIndex = 24;
            this.SellItemsLabel.Text = "販売アイテム(右のボタンで挿入削除)";
            // 
            // SellItemsListBox
            // 
            this.SellItemsListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SellItemsListBox.FormattingEnabled = true;
            this.SellItemsListBox.ItemHeight = 12;
            this.SellItemsListBox.Location = new System.Drawing.Point(19, 150);
            this.SellItemsListBox.Name = "SellItemsListBox";
            this.SellItemsListBox.Size = new System.Drawing.Size(406, 110);
            this.SellItemsListBox.TabIndex = 25;
            // 
            // whenItemOverLabel
            // 
            this.whenItemOverLabel.AutoSize = true;
            this.whenItemOverLabel.Location = new System.Drawing.Point(29, 104);
            this.whenItemOverLabel.Name = "whenItemOverLabel";
            this.whenItemOverLabel.Size = new System.Drawing.Size(90, 12);
            this.whenItemOverLabel.TabIndex = 22;
            this.whenItemOverLabel.Text = "もちものがいっぱい";
            // 
            // whenItemOverTextBox
            // 
            this.whenItemOverTextBox.Location = new System.Drawing.Point(128, 101);
            this.whenItemOverTextBox.Name = "whenItemOverTextBox";
            this.whenItemOverTextBox.Size = new System.Drawing.Size(297, 19);
            this.whenItemOverTextBox.TabIndex = 23;
            this.whenItemOverTextBox.Text = "持ち物がいっぱいですね...";
            this.whenItemOverTextBox.WordWrap = false;
            this.whenItemOverTextBox.TextChanged += new System.EventHandler(this.shopInTextBox_TextChanged);
            this.whenItemOverTextBox.Enter += new System.EventHandler(this.shopInTextBox_Enter);
            // 
            // whenNoBuyLabel
            // 
            this.whenNoBuyLabel.AutoSize = true;
            this.whenNoBuyLabel.Location = new System.Drawing.Point(29, 81);
            this.whenNoBuyLabel.Name = "whenNoBuyLabel";
            this.whenNoBuyLabel.Size = new System.Drawing.Size(53, 12);
            this.whenNoBuyLabel.TabIndex = 20;
            this.whenNoBuyLabel.Text = "非購入時";
            // 
            // whenNoBuyTextBox
            // 
            this.whenNoBuyTextBox.Location = new System.Drawing.Point(128, 78);
            this.whenNoBuyTextBox.Name = "whenNoBuyTextBox";
            this.whenNoBuyTextBox.Size = new System.Drawing.Size(297, 19);
            this.whenNoBuyTextBox.TabIndex = 21;
            this.whenNoBuyTextBox.Text = "あらら、よろしいですか";
            this.whenNoBuyTextBox.WordWrap = false;
            this.whenNoBuyTextBox.TextChanged += new System.EventHandler(this.shopInTextBox_TextChanged);
            this.whenNoBuyTextBox.Enter += new System.EventHandler(this.shopInTextBox_Enter);
            // 
            // whenBuyLabel
            // 
            this.whenBuyLabel.AutoSize = true;
            this.whenBuyLabel.Location = new System.Drawing.Point(29, 58);
            this.whenBuyLabel.Name = "whenBuyLabel";
            this.whenBuyLabel.Size = new System.Drawing.Size(41, 12);
            this.whenBuyLabel.TabIndex = 18;
            this.whenBuyLabel.Text = "購入時";
            // 
            // whenBuyTextBox
            // 
            this.whenBuyTextBox.Location = new System.Drawing.Point(128, 55);
            this.whenBuyTextBox.Name = "whenBuyTextBox";
            this.whenBuyTextBox.Size = new System.Drawing.Size(297, 19);
            this.whenBuyTextBox.TabIndex = 19;
            this.whenBuyTextBox.Text = "ありがとうございます";
            this.whenBuyTextBox.WordWrap = false;
            this.whenBuyTextBox.TextChanged += new System.EventHandler(this.shopInTextBox_TextChanged);
            this.whenBuyTextBox.Enter += new System.EventHandler(this.shopInTextBox_Enter);
            // 
            // selectItemBuyLabel
            // 
            this.selectItemBuyLabel.AutoSize = true;
            this.selectItemBuyLabel.Location = new System.Drawing.Point(17, 36);
            this.selectItemBuyLabel.Name = "selectItemBuyLabel";
            this.selectItemBuyLabel.Size = new System.Drawing.Size(78, 12);
            this.selectItemBuyLabel.TabIndex = 16;
            this.selectItemBuyLabel.Text = "アイテム選択時";
            // 
            // selectItemBuyTextBox
            // 
            this.selectItemBuyTextBox.Location = new System.Drawing.Point(128, 33);
            this.selectItemBuyTextBox.Name = "selectItemBuyTextBox";
            this.selectItemBuyTextBox.Size = new System.Drawing.Size(297, 19);
            this.selectItemBuyTextBox.TabIndex = 17;
            this.selectItemBuyTextBox.Text = "こちらでよろしいですか？";
            this.selectItemBuyTextBox.WordWrap = false;
            this.selectItemBuyTextBox.TextChanged += new System.EventHandler(this.shopInTextBox_TextChanged);
            this.selectItemBuyTextBox.Enter += new System.EventHandler(this.shopInTextBox_Enter);
            // 
            // buySelectLabel
            // 
            this.buySelectLabel.AutoSize = true;
            this.buySelectLabel.Location = new System.Drawing.Point(17, 13);
            this.buySelectLabel.Name = "buySelectLabel";
            this.buySelectLabel.Size = new System.Drawing.Size(105, 12);
            this.buySelectLabel.TabIndex = 14;
            this.buySelectLabel.Text = "買う選択時メッセージ";
            // 
            // buySelectTextBox
            // 
            this.buySelectTextBox.Location = new System.Drawing.Point(128, 10);
            this.buySelectTextBox.Name = "buySelectTextBox";
            this.buySelectTextBox.Size = new System.Drawing.Size(297, 19);
            this.buySelectTextBox.TabIndex = 15;
            this.buySelectTextBox.Text = "どちらのお品に致しましょう";
            this.buySelectTextBox.WordWrap = false;
            this.buySelectTextBox.TextChanged += new System.EventHandler(this.shopInTextBox_TextChanged);
            this.buySelectTextBox.Enter += new System.EventHandler(this.shopInTextBox_Enter);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tabPage2.Controls.Add(this.WhenCannotSellLabel);
            this.tabPage2.Controls.Add(this.whenCannotSellTextBox);
            this.tabPage2.Controls.Add(this.WhenNoSellLabel);
            this.tabPage2.Controls.Add(this.whenNoSellTextBox);
            this.tabPage2.Controls.Add(this.WhenSellLabel);
            this.tabPage2.Controls.Add(this.whenSellTextBox);
            this.tabPage2.Controls.Add(this.selectItemSellLabel);
            this.tabPage2.Controls.Add(this.selectItemSellTextBox);
            this.tabPage2.Controls.Add(this.sellSelectLabel);
            this.tabPage2.Controls.Add(this.sellSelectTextBox);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(461, 263);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "売る";
            // 
            // WhenCannotSellLabel
            // 
            this.WhenCannotSellLabel.AutoSize = true;
            this.WhenCannotSellLabel.Location = new System.Drawing.Point(29, 103);
            this.WhenCannotSellLabel.Name = "WhenCannotSellLabel";
            this.WhenCannotSellLabel.Size = new System.Drawing.Size(65, 12);
            this.WhenCannotSellLabel.TabIndex = 36;
            this.WhenCannotSellLabel.Text = "売却不可品";
            // 
            // whenCannotSellTextBox
            // 
            this.whenCannotSellTextBox.Location = new System.Drawing.Point(128, 100);
            this.whenCannotSellTextBox.Name = "whenCannotSellTextBox";
            this.whenCannotSellTextBox.Size = new System.Drawing.Size(297, 19);
            this.whenCannotSellTextBox.TabIndex = 37;
            this.whenCannotSellTextBox.Text = "ちょっとお値段のほうがつけられないですね...";
            this.whenCannotSellTextBox.WordWrap = false;
            this.whenCannotSellTextBox.TextChanged += new System.EventHandler(this.shopInTextBox_TextChanged);
            this.whenCannotSellTextBox.Enter += new System.EventHandler(this.shopInTextBox_Enter);
            // 
            // WhenNoSellLabel
            // 
            this.WhenNoSellLabel.AutoSize = true;
            this.WhenNoSellLabel.Location = new System.Drawing.Point(29, 80);
            this.WhenNoSellLabel.Name = "WhenNoSellLabel";
            this.WhenNoSellLabel.Size = new System.Drawing.Size(53, 12);
            this.WhenNoSellLabel.TabIndex = 34;
            this.WhenNoSellLabel.Text = "非売却時";
            // 
            // whenNoSellTextBox
            // 
            this.whenNoSellTextBox.Location = new System.Drawing.Point(128, 77);
            this.whenNoSellTextBox.Name = "whenNoSellTextBox";
            this.whenNoSellTextBox.Size = new System.Drawing.Size(297, 19);
            this.whenNoSellTextBox.TabIndex = 35;
            this.whenNoSellTextBox.Text = "あらら、よろしいですか";
            this.whenNoSellTextBox.WordWrap = false;
            this.whenNoSellTextBox.TextChanged += new System.EventHandler(this.shopInTextBox_TextChanged);
            this.whenNoSellTextBox.Enter += new System.EventHandler(this.shopInTextBox_Enter);
            // 
            // WhenSellLabel
            // 
            this.WhenSellLabel.AutoSize = true;
            this.WhenSellLabel.Location = new System.Drawing.Point(29, 57);
            this.WhenSellLabel.Name = "WhenSellLabel";
            this.WhenSellLabel.Size = new System.Drawing.Size(41, 12);
            this.WhenSellLabel.TabIndex = 32;
            this.WhenSellLabel.Text = "売却時";
            // 
            // whenSellTextBox
            // 
            this.whenSellTextBox.Location = new System.Drawing.Point(128, 54);
            this.whenSellTextBox.Name = "whenSellTextBox";
            this.whenSellTextBox.Size = new System.Drawing.Size(297, 19);
            this.whenSellTextBox.TabIndex = 33;
            this.whenSellTextBox.Text = "ありがとうございます";
            this.whenSellTextBox.WordWrap = false;
            this.whenSellTextBox.TextChanged += new System.EventHandler(this.shopInTextBox_TextChanged);
            this.whenSellTextBox.Enter += new System.EventHandler(this.shopInTextBox_Enter);
            // 
            // selectItemSellLabel
            // 
            this.selectItemSellLabel.AutoSize = true;
            this.selectItemSellLabel.Location = new System.Drawing.Point(17, 35);
            this.selectItemSellLabel.Name = "selectItemSellLabel";
            this.selectItemSellLabel.Size = new System.Drawing.Size(78, 12);
            this.selectItemSellLabel.TabIndex = 30;
            this.selectItemSellLabel.Text = "アイテム選択時";
            // 
            // selectItemSellTextBox
            // 
            this.selectItemSellTextBox.Location = new System.Drawing.Point(128, 32);
            this.selectItemSellTextBox.Name = "selectItemSellTextBox";
            this.selectItemSellTextBox.Size = new System.Drawing.Size(297, 19);
            this.selectItemSellTextBox.TabIndex = 31;
            this.selectItemSellTextBox.Text = "こちらでよろしいですか？";
            this.selectItemSellTextBox.WordWrap = false;
            this.selectItemSellTextBox.TextChanged += new System.EventHandler(this.shopInTextBox_TextChanged);
            this.selectItemSellTextBox.Enter += new System.EventHandler(this.shopInTextBox_Enter);
            // 
            // sellSelectLabel
            // 
            this.sellSelectLabel.AutoSize = true;
            this.sellSelectLabel.Location = new System.Drawing.Point(17, 13);
            this.sellSelectLabel.Name = "sellSelectLabel";
            this.sellSelectLabel.Size = new System.Drawing.Size(107, 12);
            this.sellSelectLabel.TabIndex = 28;
            this.sellSelectLabel.Text = "売る選択時メッセージ";
            // 
            // sellSelectTextBox
            // 
            this.sellSelectTextBox.Location = new System.Drawing.Point(128, 10);
            this.sellSelectTextBox.Name = "sellSelectTextBox";
            this.sellSelectTextBox.Size = new System.Drawing.Size(297, 19);
            this.sellSelectTextBox.TabIndex = 29;
            this.sellSelectTextBox.Text = "何を売って頂けますか";
            this.sellSelectTextBox.WordWrap = false;
            this.sellSelectTextBox.TextChanged += new System.EventHandler(this.shopInTextBox_TextChanged);
            this.sellSelectTextBox.Enter += new System.EventHandler(this.shopInTextBox_Enter);
            // 
            // shopEndTextBox
            // 
            this.shopEndTextBox.Location = new System.Drawing.Point(136, 62);
            this.shopEndTextBox.Name = "shopEndTextBox";
            this.shopEndTextBox.Size = new System.Drawing.Size(345, 19);
            this.shopEndTextBox.TabIndex = 5;
            this.shopEndTextBox.Text = "ありがとうございました～";
            this.shopEndTextBox.WordWrap = false;
            this.shopEndTextBox.TextChanged += new System.EventHandler(this.shopInTextBox_TextChanged);
            this.shopEndTextBox.Enter += new System.EventHandler(this.shopInTextBox_Enter);
            // 
            // shopEndLabel
            // 
            this.shopEndLabel.AutoSize = true;
            this.shopEndLabel.Location = new System.Drawing.Point(33, 65);
            this.shopEndLabel.Name = "shopEndLabel";
            this.shopEndLabel.Size = new System.Drawing.Size(86, 12);
            this.shopEndLabel.TabIndex = 4;
            this.shopEndLabel.Text = "退店時メッセージ";
            // 
            // shopTextBox
            // 
            this.shopTextBox.Location = new System.Drawing.Point(136, 37);
            this.shopTextBox.Name = "shopTextBox";
            this.shopTextBox.Size = new System.Drawing.Size(345, 19);
            this.shopTextBox.TabIndex = 3;
            this.shopTextBox.Text = "他に御用はありませんか?";
            this.shopTextBox.WordWrap = false;
            this.shopTextBox.TextChanged += new System.EventHandler(this.shopInTextBox_TextChanged);
            this.shopTextBox.Enter += new System.EventHandler(this.shopInTextBox_Enter);
            // 
            // shopLabel
            // 
            this.shopLabel.AutoSize = true;
            this.shopLabel.Location = new System.Drawing.Point(33, 40);
            this.shopLabel.Name = "shopLabel";
            this.shopLabel.Size = new System.Drawing.Size(98, 12);
            this.shopLabel.TabIndex = 2;
            this.shopLabel.Text = "選択終了メッセージ";
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(122, 436);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 38;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(317, 436);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 39;
            this.buttonCancel.Text = "キャンセル";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 33;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // ShopForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 471);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.shopTextBox);
            this.Controls.Add(this.shopLabel);
            this.Controls.Add(this.shopEndTextBox);
            this.Controls.Add(this.shopEndLabel);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.retLabel);
            this.Controls.Add(this.retTextBox);
            this.Controls.Add(this.sellLabel);
            this.Controls.Add(this.sellTextBox);
            this.Controls.Add(this.buyLabel);
            this.Controls.Add(this.buyTextBox);
            this.Controls.Add(this.selectMes);
            this.Controls.Add(this.shopInTextBox);
            this.Controls.Add(this.shopInLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ShopForm";
            this.Text = "お店の処理(\'/\'使用禁止 改行は\\nを入力してください。)";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label shopInLabel;
        private System.Windows.Forms.TextBox shopInTextBox;
        private System.Windows.Forms.TextBox buyTextBox;
        private System.Windows.Forms.Label selectMes;
        private System.Windows.Forms.Label buyLabel;
        private System.Windows.Forms.Label sellLabel;
        private System.Windows.Forms.TextBox sellTextBox;
        private System.Windows.Forms.Label retLabel;
        private System.Windows.Forms.TextBox retTextBox;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox shopEndTextBox;
        private System.Windows.Forms.Label shopEndLabel;
        private System.Windows.Forms.Label buySelectLabel;
        private System.Windows.Forms.TextBox buySelectTextBox;
        private System.Windows.Forms.Label sellSelectLabel;
        private System.Windows.Forms.TextBox sellSelectTextBox;
        private System.Windows.Forms.Label whenItemOverLabel;
        private System.Windows.Forms.TextBox whenItemOverTextBox;
        private System.Windows.Forms.Label whenNoBuyLabel;
        private System.Windows.Forms.TextBox whenNoBuyTextBox;
        private System.Windows.Forms.Label whenBuyLabel;
        private System.Windows.Forms.TextBox whenBuyTextBox;
        private System.Windows.Forms.Label selectItemBuyLabel;
        private System.Windows.Forms.TextBox selectItemBuyTextBox;
        private System.Windows.Forms.Label WhenCannotSellLabel;
        private System.Windows.Forms.TextBox whenCannotSellTextBox;
        private System.Windows.Forms.Label WhenNoSellLabel;
        private System.Windows.Forms.TextBox whenNoSellTextBox;
        private System.Windows.Forms.Label WhenSellLabel;
        private System.Windows.Forms.TextBox whenSellTextBox;
        private System.Windows.Forms.Label selectItemSellLabel;
        private System.Windows.Forms.TextBox selectItemSellTextBox;
        private System.Windows.Forms.TextBox shopTextBox;
        private System.Windows.Forms.Label shopLabel;
        private System.Windows.Forms.Label SellItemsLabel;
        private System.Windows.Forms.ListBox SellItemsListBox;
        private System.Windows.Forms.Button SubItemButton;
        private System.Windows.Forms.Button AddItemButton;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Timer timer1;
    }
}