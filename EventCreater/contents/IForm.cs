﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class IForm : Form
    {
        int[] Args = new int[5];
        EventContentsForm ecf;
        ComponentsSync arsyn;
        public IForm(Form parent, int mode = 0, int[] e_Args = null)
        {
            // 個々のイベントコンテンツの共通処理
            InitializeComponent();

            Item.ItemLoadAndSet(comboBox1);

            ecf = new EventContentsForm(this, mode);

            object[] radios = { radioButton6, radioButton7, radioButton7,
                                radioButton2, radioButton1, radioButton1};
            object[] boxs = { comboBox1, label1, button1,
                              numericUpDown2, label4, button2};

            arsyn = new ComponentsSync(radios, boxs);
            SetArgsToForm(e_Args);
            // うげ、同期しない...。
            arsyn.async();
        }

        private void SetArgsToForm(int[] ags)
        {
            if (ags == null) return;
            RadioButton[] rb1 = { radioButton4, radioButton5 };
            RadioButton[] rb2 = { radioButton6, radioButton7 };
            RadioButton[] rb3 = { radioButton2, radioButton1 };
            rb1[ags[0]].Checked = true;
            rb2[ags[1]].Checked = true;
            rb3[ags[3]].Checked = true;
            if (ags[1] == 0) comboBox1.SelectedIndex = ags[2];
            else label1.Text = Variable.GetVariableLabelText(ags[2]);

            if (ags[3] == 0) numericUpDown2.Value = ags[4];
            else label4.Text = Variable.GetVariableLabelText(ags[4]);
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Args[0] = (radioButton4.Checked == true) ? 0 : 1;
            Args[1] = (radioButton6.Checked == true) ? 0 : 1;
            Args[2] = (radioButton6.Checked == true) ? comboBox1.SelectedIndex : Variable.GetVariableIndex(label1);
            Args[3] = (radioButton2.Checked == true) ? 0 : 1;
            Args[4] = (radioButton2.Checked == true) ? (int)numericUpDown2.Value : Variable.GetVariableIndex(label4);

            string[] code = new string[1];
            string str = "";
            str = (radioButton6.Checked == true) ? "\"アイテム_" + comboBox1.SelectedItem : str;
            str = (radioButton7.Checked == true) ? "\"変数_" + label1.Text + "のアイテム" : str;
            str += "\",";

            code[0] = "Item(" + str + Args[0] + "," + Args[1] + "," + Args[2] + "," + Args[3] + "," + Args[4] + ")";
            ecf.buttonOK_Close(code);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ecf.buttonCansel_Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, label1, (int a, string vnamecomp) =>
            {
                label1.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, label4, (int a, string vnamecomp) =>
            {
                label4.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private bool shift = false;
        private bool ctrl = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            shift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift);
            ctrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl);

            if (shift == true && ctrl == true)
                button3_Click(null, null);
        }
    }
}
