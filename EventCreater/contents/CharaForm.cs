﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class CharaForm : Form
    {
        int[] Args = new int[10];
        EventContentsForm ecf;
        public CharaForm(Form parent, int mode = 0, int[] e_Args = null)
        {
            // 個々のイベントコンテンツの共通処理
            InitializeComponent();
            VisualToNum();

            Variable.CharaNameLoadAndSet(comboBox1);
            Variable.CharaFaceLoadAndSet(comboBox2);
            Variable.CharaViewLoadAndSet(comboBox3);

            ecf = new EventContentsForm(this, mode);
            SetArgsToForm(e_Args);
        }

        private void SetArgsToForm(int[] ags)
        {
            if (ags == null) return;
            comboBox1.SelectedIndex = ags[0];
            comboBox2.SelectedIndex = ags[1];
            numericUpDown1.Value = ags[2];
            numericUpDown2.Value = ags[3];
            numericUpDownz.Value = ags[4];
            numericUpDown3.Value = ags[5];
            numericUpDown4.Value = ags[6];
            comboBox3.SelectedIndex = ags[7];
            numericUpDown5.Value = ags[8];
            checkBox1.Checked = Convert.ToBoolean(ags[9]);
        }
        bool _isDraging = false;
        Point? _diffPoint = null;

        private void labelChara_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            {
                return;
            }
            Cursor.Current = Cursors.Hand;
            _isDraging = true;
            _diffPoint = e.Location;

            panel.Size = labelChara.Size;
            panel.Location = labelChara.Location;

            panel.Visible = true;
        }

        private void labelChara_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_isDraging)
            {
                return;
            }

            int x = labelChara.Location.X + e.X - _diffPoint.Value.X;
            int y = labelChara.Location.Y + e.Y - _diffPoint.Value.Y;

            if (x <= 0) x = 0;
            if (y <= 0) y = 0;

            if (x >= 230) x = 229;
            if (y >= 125) y = 124;

            panel.Location = new Point(x, y);
        }

        private void labelChara_MouseUp(object sender, MouseEventArgs e)
        {
            Cursor.Current = Cursors.Default;
            _isDraging = false;

            panel.Visible = false;

            if (e.Button != MouseButtons.Left)
            {
                return;
            }

            labelChara.Location = panel.Location;
            VisualToNum();
        }

        /// <summary>
        /// ラベルドラッグ後に座標数値を数値ボックスに代入。
        /// </summary>
        private void VisualToNum()
        {
            Point pos = labelChara.Location;
            numericUpDown1.Value = pos.X * 8;
            numericUpDown2.Value = pos.Y * 8;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            NumToVisual();
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            NumToVisual();
        }

        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {
            NumToVisual();
        }

        Size default_size = new Size(60, 90);
        /// <summary>
        /// 座標数値をボックスから変更したときラベルの表示を変更。
        /// </summary>
        private void NumToVisual()
        {
            labelChara.Location = new Point((int)numericUpDown1.Value / 8, (int)numericUpDown2.Value / 8);
            Size s = new Size(default_size.Width * (int)numericUpDown4.Value / 10, default_size.Height * (int)numericUpDown4.Value / 10);
            labelChara.Size = s;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            string str = "\"" + (string)comboBox1.SelectedItem + "\",";
            Args[0] = comboBox1.SelectedIndex;
            Args[1] = comboBox2.SelectedIndex;
            Args[2] = (int)numericUpDown1.Value;
            Args[3] = (int)numericUpDown2.Value;
            Args[4] = (int)numericUpDownz.Value;
            Args[5] = (int)numericUpDown3.Value;
            Args[6] = (int)numericUpDown4.Value;
            Args[7] = comboBox3.SelectedIndex;
            Args[8] = (int)numericUpDown5.Value;
            Args[9] = Convert.ToInt32(checkBox1.Checked);

            string[] code = new string[1];
            code[0] = "CharaView(" + str + Args[0] + "," + Args[1] + "," + Args[2] + "," + Args[3] + "," + Args[4] + ","
                                   + Args[5] + "," + Args[6] + "," + Args[7] + "," + Args[8] + "," + Args[9] + ")"; 
            ecf.buttonOK_Close(code);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ecf.buttonCansel_Close();
        }

        private bool shift = false;
        private bool ctrl = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            shift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift);
            ctrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl);

            if (shift == true && ctrl == true)
                button1_Click(null, null);
        }
        
        private void buttonCharaX_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(8, null, (int a, string vnamecomp) =>
            {
                try
                {
                    string str_location = vnamecomp.Split(' ')[1];
                    numericUpDown1.Value = decimal.Parse(str_location);
                }
                catch
                {
                    MessageBox.Show("座標値の入力形式が正しくないようです。");
                }
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private void buttonCharaY_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(9, null, (int a, string vnamecomp) =>
            {
                try
                {
                    string str_location = vnamecomp.Split(' ')[1];
                    numericUpDown2.Value = decimal.Parse(str_location);
                }
                catch
                {
                    MessageBox.Show("座標値の入力形式が正しくないようです。");
                }
            });
            CloseAct.FormOpenCommon(this, vf);
        }
    }

}
