﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class EventCallForm : Form
    {
        private string[] labels;
        EventContentsForm ecf;
        public EventCallForm(Form parent, int mode = 0, string e_Str = null)
        {
            // 個々のイベントコンテンツの共通処理
            InitializeComponent();
            SetComboBoxItem();

            SetArgsToForm(e_Str);
            ecf = new EventContentsForm(this, mode);
        }

        string[] view;
        string[] lbls;
        private void SetComboBoxItem()
        {
            view = new string[0];
            lbls = new string[0];
            EventData.LoadEventListFormFile();
            EventData.BufToEventNameAndLabels(ref view, ref lbls);
            labels = lbls;
            foreach (var a in view)
                comboBox1.Items.Add(a);

            if (comboBox1.Items.Count > 0)
                comboBox1.SelectedIndex = 0;
            else
                comboBox1.Enabled = false;
        }

        private void SetArgsToForm(string str)
        {
            if (str == null) return;
            var label = str.Split(',')[0];

            for(int i = 0; i < lbls.Length; i++)
                if (lbls[i] == label)
                {
                    comboBox1.SelectedIndex = i;
                    return;
                }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex < 0 || string.IsNullOrEmpty(eventLabel.Text))
            {
                MessageBox.Show("イベントを選択してください。");
                return;
            }

            string[] code = { "Call(\"" + eventLabel.Text + "," +(string)comboBox1.Items[comboBox1.SelectedIndex] + "\")" };
            ecf.buttonOK_Close(code);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ecf.buttonCansel_Close();
        }

        private bool shift = false;
        private bool ctrl = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            shift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift);
            ctrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl);

            if (shift == true && ctrl == true)
                button1_Click(null, null);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                eventLabel.Text = lbls[comboBox1.SelectedIndex];
            }
            catch
            {
                eventLabel.Text = string.Empty;
            }
        }
    }
}
