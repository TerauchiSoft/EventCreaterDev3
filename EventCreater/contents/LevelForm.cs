﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class LevelForm : Form
    {
        int[] Args = new int[6];
        EventContentsForm ecf;
        ComponentsSync arsyn;
        public LevelForm(Form parent, int mode = 0, int[] e_Args = null)
        {
            // 個々のイベントコンテンツの共通処理
            InitializeComponent();

            ecf = new EventContentsForm(this, mode);

            Variable.CharaNameLoadAndSet(comboBox1);

            // 各種オブジェクトをラジオボタンと連動させる。
            object[] radios = new object[7];
            object[] boxs = new object[7];

            radios[0] = radioButton1;
            radios[1] = radioButton2;
            radios[2] = radioButton3;
            radios[3] = radioButton3;
            radios[4] = radioButton6;
            radios[5] = radioButton7;
            radios[6] = radioButton7;

            boxs[0] = null;
            boxs[1] = comboBox1;
            boxs[2] = label4;
            boxs[3] = button4;
            boxs[4] = numericUpDown1;
            boxs[5] = label1;
            boxs[6] = button1;

            arsyn = new ComponentsSync(radios, boxs);
            SetArgsToForm(e_Args);
        }

        private void SetArgsToForm(int[] ags)
        {
            if (ags == null) return;
            RadioButton[] rb1 = { radioButton1, radioButton2, radioButton3 };
            RadioButton[] rb2 = { radioButton4, radioButton5 };
            RadioButton[] rb3 = { radioButton6, radioButton7 };
            rb1[ags[0]].Checked = true;
            rb2[ags[2]].Checked = true;
            rb3[ags[3]].Checked = true;
            if (ags[0] == 1) comboBox1.SelectedIndex = ags[1];
            else if (ags[0] == 2) label4.Text = Variable.GetVariableLabelText(ags[1]);

            if (ags[3] == 0) numericUpDown1.Value = ags[4];
            else label1.Text = Variable.GetVariableLabelText(ags[4]);

            checkBox1.Checked = Convert.ToBoolean(ags[5]);
        }

        /// <summary>
        /// 条件変数選択フォームを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, label1, (int a, string vnamecomp) =>
            {
                // 0~ 無くてもいいんだが。
                Args[4] = a;
                label1.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, label4, (int a, string vnamecomp) =>
            {
                // 0~ 無くてもいいんだが。
                Args[1] = a;
                label4.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ecf.buttonCansel_Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string[] code = new string[1];
            Args[0] = (radioButton1.Checked == true) ? 0 : Args[0];
            Args[0] = (radioButton2.Checked == true) ? 1 : Args[0];
            Args[0] = (radioButton3.Checked == true) ? 2 : Args[0];
            Args[1] = (comboBox1.Enabled == true) ? comboBox1.SelectedIndex : Args[1];
            Args[1] = (label4.Enabled == true) ? Variable.GetVariableIndex(label4) : Args[1];
            Args[2] = (radioButton4.Checked == true) ? 0 : 1;
            Args[3] = (radioButton6.Checked == true) ? 0 : 1;
            Args[4] = (numericUpDown1.Enabled == true) ? (int)numericUpDown1.Value : Variable.GetVariableIndex(label1);
            Args[5] = (checkBox1.Checked == true) ? 1 : 0;
            code[0] = "LV(" + Args[0] + "," + Args[1] + "," + Args[2] + "," + Args[3] + "," + Args[4] + "," + Args[5] + ")";

            ecf.buttonOK_Close(code);
        }

        private bool shift = false;
        private bool ctrl = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            shift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift);
            ctrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl);

            if (shift == true && ctrl == true)
                button2_Click(null, null);
        }
    }
}