﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class EffectForm : Form
    {
        EventContentsForm ecf;
        public EffectForm(Form parent, int mode = 0, int[] e_Args = null)
        {
            // 個々のイベントコンテンツの共通処理
            InitializeComponent();
            SetArgsToForm(e_Args);

            ecf = new EventContentsForm(this, mode);
            ecf.parentForm = parent;
        }

        private void SetArgsToForm(int[] ags)
        {
            if (ags == null) return;

            // numericUpDown1.Value = ags[0];
        }

    }
}
