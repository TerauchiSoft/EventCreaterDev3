﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class CharaDelForm : Form
    {
        int[] Args = new int[4];
        EventContentsForm ecf;
        public CharaDelForm(Form parent, int mode = 0, int[] e_Args = null)
        {
            // 個々のイベントコンテンツの共通処理
            InitializeComponent();

            Variable.CharaViewOutLoadAndSet(comboBox1);
            Variable.CharaNameLoadAndSet(comboBoxChara);

            ecf = new EventContentsForm(this, mode);
            SetArgsToForm(e_Args);
        }


        private void SetArgsToForm(int[] ags)
        {
            if (ags == null) return;
            comboBoxChara.SelectedIndex = ags[0];
            comboBox1.SelectedIndex = ags[1];
            numericUpDown5.Value = ags[2];
            checkBox1.Checked = Convert.ToBoolean(ags[3]);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string str = "\"" + comboBoxChara.SelectedItem + "\",";
            Args[0] = (int)comboBoxChara.SelectedIndex;
            Args[1] = comboBox1.SelectedIndex;
            Args[2] = (int)numericUpDown5.Value;
            Args[3] = Convert.ToInt32(checkBox1.Checked);

            string[] code = new string[1];
            code[0] = "CharaDelete(" + str + Args[0] + "," + Args[1] + "," + Args[2] + "," + Args[3] + ")"; 
            ecf.buttonOK_Close(code);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ecf.buttonCansel_Close();
        }

        private bool shift = false;
        private bool ctrl = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            shift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift);
            ctrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl);

            if (shift == true && ctrl == true)
                button1_Click(null, null);
        }
    }

}
