﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class FlagForm : Form
    {
        int[] Args = new int[4];
        EventContentsForm ecf;
        ComponentsSync arsyn;
        public FlagForm(Form parent, int mode = 0, int[] e_Args = null)
        {
            // 個々のイベントコンテンツの共通処理
            InitializeComponent();

            ecf = new EventContentsForm(this, mode);
            ecf.parentForm = parent;

            object[] radios = { radioButton1, radioButton1,
                                radioButton2, radioButton2,
                                radioButton3, radioButton3 };

            object[] boxs = { label1, button1,
                              numericUpDown1, numericUpDown2,
                              label2, button2 };

            arsyn = new ComponentsSync(radios, boxs);
            SetArgsToForm(e_Args);
        }

        private void SetArgsToForm(int[] ags)
        {
            if (ags == null) return;
            RadioButton[] rb1 = { radioButton1, radioButton2, radioButton3 };
            RadioButton[] rb2 = { radioButton4, radioButton5, radioButton6 };

            if (ags[0] == 0) label1.Text = Variable.GetFlagLabelText(ags[1]);
            else if (ags[0] == 2) label2.Text = Variable.GetVariableLabelText(ags[1]);
            else
            {
                numericUpDown1.Value = ags[1];
                numericUpDown2.Value = ags[2];
            }

            rb1[ags[0]].Checked = true;
            rb2[ags[3]].Checked = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Args[0] = (radioButton1.Checked == true) ? 0 : Args[0];
            Args[0] = (radioButton2.Checked == true) ? 1 : Args[0];
            Args[0] = (radioButton3.Checked == true) ? 2 : Args[0];

            Args[1] = (radioButton1.Checked == true) ? Variable.GetVariableIndex(label1) : Args[1];
            Args[1] = (radioButton2.Checked == true) ? (int)numericUpDown1.Value : Args[1];
            Args[1] = (radioButton3.Checked == true) ? Variable.GetVariableIndex(label2) : Args[1];

            Args[2] = (radioButton2.Checked == true) ? (int)numericUpDown2.Value : 0;

            Args[3] = (radioButton4.Checked == true) ? 0 : Args[3];
            Args[3] = (radioButton5.Checked == true) ? 1 : Args[3];
            Args[3] = (radioButton6.Checked == true) ? 2 : Args[3];

            string str = "";
            str = (radioButton1.Checked == true) ? "\"フラグ_" + label1.Text : str;
            str = (radioButton2.Checked == true) ? "\"フラグ_" + numericUpDown1.Value + "~" + numericUpDown2.Value + "まで" : str;
            str = (radioButton3.Checked == true) ? "\"変数_" + label2.Text + "の指すフラグ" : str;
            str += "\",";

            string[] code = new string[1];
            code[0] = "Flag(" + str + Args[0] + "," + Args[1] + "," + Args[2] + "," + Args[3] + ")";

            ecf.buttonOK_Close(code);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(0, label1, (int a, string vnamecomp) =>
            {
                label1.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, label2, (int a, string vnamecomp) =>
            {
                label2.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private bool shift = false;
        private bool ctrl = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            shift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift);
            ctrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl);

            if (shift == true && ctrl == true)
                button3_Click(null, null);
        }
    }
}
