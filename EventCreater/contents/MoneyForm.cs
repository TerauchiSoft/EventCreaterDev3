﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class MoneyForm : Form
    {
        int[] Args = new int[3];
        EventContentsForm ecf;
        ComponentsSync arsyn;
        public MoneyForm(Form parent, int mode = 0, int[] e_Args = null)
        {
            // 個々のイベントコンテンツの共通処理
            InitializeComponent();

            ecf = new EventContentsForm(this, mode);

            object[] radios = { radioButton6, radioButton7, radioButton7 };
            object[] boxs = { numericUpDown1, label1, button1 };

            arsyn = new ComponentsSync(radios, boxs);
            SetArgsToForm(e_Args);
            // うげ、同期しない...。
            arsyn.async();
        }

        private void SetArgsToForm(int[] ags)
        {
            if (ags == null) return;
            RadioButton[] rb1 = { radioButton4, radioButton5 };
            RadioButton[] rb2 = { radioButton6, radioButton7 };

            rb1[ags[0]].Checked = true;
            rb2[ags[1]].Checked = true;

            if (ags[1] == 0)
                numericUpDown1.Value = ags[2];
            else
                label1.Text = Variable.GetVariableLabelText(ags[2]);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Args[0] = (radioButton4.Checked == true) ? 0 : 1;
            Args[1] = (radioButton6.Checked == true) ? 0 : 1;
            Args[2] = (radioButton6.Checked == true) ? (int)numericUpDown1.Value : Variable.GetVariableIndex(label1);

            string[] code = new string[1];
            code[0] = "Money(" + Args[0] + "," + Args[1] + "," + Args[2] + ")";
            ecf.buttonOK_Close(code);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ecf.buttonCansel_Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, label1, (int a, string vnamecomp) =>
            {
                label1.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private bool shift = false;
        private bool ctrl = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            shift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift);
            ctrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl);

            if (shift == true && ctrl == true)
                button2_Click(null, null);
        }
    }
}
