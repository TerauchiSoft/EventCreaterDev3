﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class CharaBackForm : Form
    {
        EventContentsForm ecf;
        public CharaBackForm(Form parent, int mode = 0, int[] e_Args = null)
        {
            InitializeComponent();
            ecf = new EventContentsForm(this, mode);
            ecf.parentForm = parent;
            Variable.CharaNameLoadAndSet(comboBox1);
            SetArgsToForm(e_Args);
        }
        private void SetArgsToForm(int[] ags)
        {
            if (ags == null) return;
            comboBox1.SelectedIndex = ags[0];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[] code = new string[1];
            code[0] = "CharaBack(\"" + comboBox1.SelectedItem + "\"," + comboBox1.SelectedIndex.ToString() + ")";
            ecf.buttonOK_Close(code.ToArray());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ecf.buttonCansel_Close();
        }
    }
}

