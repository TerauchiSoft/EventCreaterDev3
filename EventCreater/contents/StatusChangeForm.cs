﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class StatusChangeForm : Form
    {
        EventContentsForm ecf;
        ComponentsSync async;
        public StatusChangeForm(Form parent, int mode = 0, int[] e_Args = null)
        {
            InitializeComponent();
            Variable.CharaNameLoadAndSet(comboBox1);
            Variable.StatusLoadAndSet(StatusComboBox);

            SetArgs(e_Args);

            object[] radios = { radioButton1, radioButton2, radioButton3, radioButton3, radioButton4, radioButton5 };
            object[] boxs = { null, comboBox1, label4, button4, StatusComboBox, null };
            async = new ComponentsSync(radios, boxs);
            ecf = new EventContentsForm(this, mode);
        }

        private void SetArgs(int[] args)
        {
            if (args == null)
                return;

            RadioButton[] rbs = { radioButton1, radioButton2, radioButton3 };
            rbs[args[0]].Checked = true;
            if (args[0] == 1)
                comboBox1.SelectedIndex = args[1];
            else if (args[0] == 2)
                label4.Text = Variable.GetVariableLabelText(args[1]);

            if (args[2] == 0)
                radioButton4.Checked = true;
            else
                radioButton5.Checked = true;
            StatusComboBox.SelectedIndex = args[3];
        }

        private void button4_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, label4, (int a, string vnamecomp) =>
            {
                label4.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {

            string[] code = { "Condition(" };
            int ag1 = 0;
            int ag2 = 0;
            int ag3 = 0;
            int ag4 = 0;

            if (radioButton1.Checked == true)
                ag1 = 0;
            else if (radioButton2.Checked == true) {
                ag1 = 1;
                ag2 = comboBox1.SelectedIndex;
            }
            else{
                ag1 = 2;
                ag2 = Variable.GetVariableIndex(label4);
            }

            if (radioButton4.Checked == true)
                ag3 = 0;
            else
                ag3 = 1;
            ag4 = StatusComboBox.SelectedIndex;

            code[0] += ag1 + "," + ag2 + "," + ag3 + "," + ag4 + ")";
            ecf.buttonOK_Close(code);
        }

        private void buttonCansel_Click(object sender, EventArgs e)
        {
            ecf.buttonCansel_Close();
        }

        private bool shift = false;
        private bool ctrl = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            shift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift);
            ctrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl);

            if (shift == true && ctrl == true)
                buttonOK_Click(null, null);
        }
    }
}
