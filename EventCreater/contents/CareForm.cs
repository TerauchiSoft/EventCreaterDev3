﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class CareForm : Form
    {
        int[] Args = new int[2];
        EventContentsForm ecf;
        ComponentsSync arsyn;
        public CareForm(Form parent, int mode = 0, int[] e_Args = null)
        {
            // 個々のイベントコンテンツの共通処理
            InitializeComponent();

            ecf = new EventContentsForm(this, mode);
            ecf.parentForm = parent;

            Variable.CharaNameLoadAndSet(comboBox1);

            object[] radios = new object[4];
            object[] boxs = new object[4];

            radios[0] = radioButton1;
            radios[1] = radioButton2;
            radios[2] = radioButton3;
            radios[3] = radioButton3;

            boxs[0] = null;
            boxs[1] = comboBox1;
            boxs[2] = label1;
            boxs[3] = button1;

            arsyn = new ComponentsSync(radios, boxs);
            SetArgsToForm(e_Args);
        }

        private void SetArgsToForm(int[] ags)
        {
            if (ags == null) return;
            if (ags[0] == 0)
            {
                radioButton1.Checked = true;
            }
            else if (ags[0] == 1)
            {
                radioButton2.Checked = true;
                comboBox1.SelectedIndex = ags[1];
            }
            else if (ags[0] == 2)
            {
                radioButton3.Checked = true;
                Variable.ReadVariableFile(Variable.fname[1]);
                label1.Text = Variable.GetVariableLabelText(ags[1]);
            }
        }

        private void Cansel_Click(object sender, EventArgs e)
        {
            ecf.buttonCansel_Close();
        }

        private void OK_Click(object sender, EventArgs e)
        {
            Args[0] = (radioButton1.Checked == true) ? 0 : Args[0];
            Args[0] = (radioButton2.Checked == true) ? 1 : Args[0];
            Args[0] = (radioButton3.Checked == true) ? 2 : Args[0];

            Args[1] = (radioButton1.Checked == true) ? 0 : Args[1];
            Args[1] = (radioButton2.Checked == true) ? comboBox1.SelectedIndex : Args[1];
            Args[1] = (radioButton3.Checked == true) ? Variable.GetVariableIndex(label1) : Args[1];

            string[] code = new string[1];
           
            code[0] = "FullCare(" + Args[0] + "," + Args[1] + ")";

            ecf.buttonOK_Close(code);
        }

        private void Variable_Click_1(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, label1, (int a, string vnamecomp) =>
            {
                label1.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private bool shift = false;
        private bool ctrl = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            shift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift);
            ctrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl);

            if (shift == true && ctrl == true)
                OK_Click(null, null);
        }
    }

}
