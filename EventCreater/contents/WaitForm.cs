﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class WaitForm : Form
    {
        EventContentsForm ecf;
        public WaitForm(Form parent, int mode = 0, int[] e_Args = null)
        {
            InitializeComponent();
            ecf = new EventContentsForm(this, mode);
            ecf.parentForm = parent;
            SetArgsToForm(e_Args);
        }
        private void SetArgsToForm(int[] ags)
        {
            if (ags == null) return;
            numericUpDown1.Value = ags[0];
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[] code = new string[1];
            code[0] = "Wait(" + numericUpDown1.Value.ToString() + ")";
            ecf.buttonOK_Close(code.ToArray());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ecf.buttonCansel_Close();
        }
    }
}
