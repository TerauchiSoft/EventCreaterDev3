﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class SkillForm : Form
    {
        int[] Args = new int[5];
        EventContentsForm ecf;
        ComponentsSync arsyn;
        public SkillForm(Form parent, int mode = 0, int[] e_Args = null)
        {
            // 個々のイベントコンテンツの共通処理
            InitializeComponent();

            Item.SkillLoadAndSet(comboBoxActorCondSecond);
            Variable.CharaNameLoadAndSet(comboBox1);

            ecf = new EventContentsForm(this, mode);
            ecf.parentForm = parent;

            object[] radios = { radioButton1, radioButton2, radioButton3, radioButton3,
                                radioButton6, radioButton7, radioButton7};
            object[] boxs = { null, comboBox1, label4, button4,
                              comboBoxActorCondSecond, button1, label1};

            arsyn = new ComponentsSync(radios, boxs);
            SetArgsToForm(e_Args);
        }

        private void SetArgsToForm(int[] ags)
        {
            if (ags == null) return;
            RadioButton[] rb1 = { radioButton1, radioButton2, radioButton3 };
            RadioButton[] rb2 = { radioButton4, radioButton5 };
            RadioButton[] rb3 = { radioButton6, radioButton7 };
            rb1[ags[0]].Checked = true;
            rb2[ags[2]].Checked = true;
            rb3[ags[3]].Checked = true;

            if (ags[0] == 1)
                comboBox1.SelectedIndex = ags[1];
            else if (ags[0] == 2)
                label4.Text = Variable.GetVariableLabelText(ags[1]);

            if (ags[3] == 0)
                comboBoxActorCondSecond.SelectedIndex = ags[4];
            else
                label1.Text = Variable.GetVariableLabelText(ags[4]);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Args[0] = (radioButton1.Checked == true) ? 0 : Args[0];
            Args[0] = (radioButton2.Checked == true) ? 1 : Args[0];
            Args[0] = (radioButton3.Checked == true) ? 2 : Args[0];

            Args[1] = (radioButton2.Checked == true) ? comboBox1.SelectedIndex : Variable.GetVariableIndex(label4);
            Args[1] = (radioButton1.Checked) ? 0 : Args[1];

            Args[2] = (radioButton4.Checked == true) ? 0 : 1;

            Args[3] = (radioButton6.Checked == true) ? 0 : 1;

            Args[4] = (radioButton6.Checked == true) ? comboBoxActorCondSecond.SelectedIndex : Variable.GetVariableIndex(label1);
            
            string[] code = new string[1];
            code[0] = "Skill(" + Args[0] + "," + Args[1] + "," + Args[2] + "," + Args[3] + "," + Args[4] + "," + ")";
            ecf.buttonOK_Close(code);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ecf.buttonCansel_Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, label4, (int a, string vnamecomp) =>
            {
                label4.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            VariableForm vf = new VariableForm(1, label1, (int a, string vnamecomp) =>
            {
                label1.Text = vnamecomp;
            });
            CloseAct.FormOpenCommon(this, vf);
        }

        private bool shift = false;
        private bool ctrl = false;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // 誤動作防止
            if (Form.ActiveForm != this)
                return;

            shift = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftShift) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightShift);
            ctrl = System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.LeftCtrl) ||
                    System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.RightCtrl);

            if (shift == true && ctrl == true)
                button3_Click(null, null);
        }
    }
}
