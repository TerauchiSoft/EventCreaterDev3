﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class VariableMaxChangeForm : Form
    {
        private string path;
        private int val;

        public VariableMaxChangeForm(string pt)
        {
            InitializeComponent();
            path = pt;
        }

        /// <summary>
        /// ロード時Variableから変数の数を読み込む
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VariableMaxChanged_Load(object sender, EventArgs e)
        {
            val = Variable.NumVariable;
            numericUpDown1.Value = val;
        }

        /// <summary>
        /// 値変更時、バッファに値を保存する。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown a = (NumericUpDown)sender;
            val = (int)a.Value;
        }

        /// <summary>
        /// ただ閉じる。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// 保存して閉じる。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {
            val = (int)numericUpDown1.Value;
            Variable.NumVariableChange(val, path);
            this.Close();
        }
    }
}
