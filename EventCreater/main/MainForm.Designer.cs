﻿namespace EventCreater
{
    partial class EventList
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxMainEvent = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonEventCreater = new System.Windows.Forms.Button();
            this.buttonFlagCreater = new System.Windows.Forms.Button();
            this.buttonVariableCreater = new System.Windows.Forms.Button();
            this.buttonItemCreater = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.buttonSkillList = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.buttonSaveToFile = new System.Windows.Forms.Button();
            this.buttonTODO = new System.Windows.Forms.Button();
            this.buttonFindSound = new System.Windows.Forms.Button();
            this.buttonUpdateTable = new System.Windows.Forms.Button();
            this.buttonRemoteToLocal = new System.Windows.Forms.Button();
            this.buttonLocalToRemote = new System.Windows.Forms.Button();
            this.buttonEventTag = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBoxMainEvent
            // 
            this.listBoxMainEvent.FormattingEnabled = true;
            this.listBoxMainEvent.HorizontalScrollbar = true;
            this.listBoxMainEvent.ItemHeight = 12;
            this.listBoxMainEvent.Location = new System.Drawing.Point(22, 46);
            this.listBoxMainEvent.Name = "listBoxMainEvent";
            this.listBoxMainEvent.Size = new System.Drawing.Size(338, 424);
            this.listBoxMainEvent.TabIndex = 0;
            this.listBoxMainEvent.DoubleClick += new System.EventHandler(this.listBoxMainEvent_DoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "イベント一覧";
            // 
            // buttonEventCreater
            // 
            this.buttonEventCreater.Location = new System.Drawing.Point(411, 94);
            this.buttonEventCreater.Name = "buttonEventCreater";
            this.buttonEventCreater.Size = new System.Drawing.Size(109, 27);
            this.buttonEventCreater.TabIndex = 2;
            this.buttonEventCreater.Text = "新規イベント作成";
            this.buttonEventCreater.UseVisualStyleBackColor = true;
            this.buttonEventCreater.Click += new System.EventHandler(this.buttonEventCreater_Click);
            // 
            // buttonFlagCreater
            // 
            this.buttonFlagCreater.Location = new System.Drawing.Point(411, 136);
            this.buttonFlagCreater.Name = "buttonFlagCreater";
            this.buttonFlagCreater.Size = new System.Drawing.Size(109, 27);
            this.buttonFlagCreater.TabIndex = 3;
            this.buttonFlagCreater.Text = "フラグリスト";
            this.buttonFlagCreater.UseVisualStyleBackColor = true;
            this.buttonFlagCreater.Click += new System.EventHandler(this.buttonFlagCreater_Click);
            // 
            // buttonVariableCreater
            // 
            this.buttonVariableCreater.Location = new System.Drawing.Point(411, 181);
            this.buttonVariableCreater.Name = "buttonVariableCreater";
            this.buttonVariableCreater.Size = new System.Drawing.Size(109, 27);
            this.buttonVariableCreater.TabIndex = 4;
            this.buttonVariableCreater.Text = "変数リスト";
            this.buttonVariableCreater.UseVisualStyleBackColor = true;
            this.buttonVariableCreater.Click += new System.EventHandler(this.buttonVariableCreater_Click);
            // 
            // buttonItemCreater
            // 
            this.buttonItemCreater.Location = new System.Drawing.Point(411, 223);
            this.buttonItemCreater.Name = "buttonItemCreater";
            this.buttonItemCreater.Size = new System.Drawing.Size(109, 27);
            this.buttonItemCreater.TabIndex = 5;
            this.buttonItemCreater.Text = "アイテムリスト";
            this.buttonItemCreater.UseVisualStyleBackColor = true;
            this.buttonItemCreater.Click += new System.EventHandler(this.buttonItemCreater_Click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("MS UI Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button5.Location = new System.Drawing.Point(411, 334);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(46, 27);
            this.button5.TabIndex = 6;
            this.button5.Text = "♫";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("MS UI Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button6.Location = new System.Drawing.Point(474, 334);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(46, 27);
            this.button6.TabIndex = 7;
            this.button6.Text = "🔊";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // buttonSkillList
            // 
            this.buttonSkillList.Location = new System.Drawing.Point(411, 267);
            this.buttonSkillList.Name = "buttonSkillList";
            this.buttonSkillList.Size = new System.Drawing.Size(109, 27);
            this.buttonSkillList.TabIndex = 8;
            this.buttonSkillList.Text = "スキルリスト";
            this.buttonSkillList.UseVisualStyleBackColor = true;
            this.buttonSkillList.Click += new System.EventHandler(this.buttonSkillList_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button1.Location = new System.Drawing.Point(411, 367);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(46, 27);
            this.button1.TabIndex = 9;
            this.button1.Text = "キャラ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button2.Location = new System.Drawing.Point(474, 367);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(46, 27);
            this.button2.TabIndex = 10;
            this.button2.Text = "状態";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // buttonSaveToFile
            // 
            this.buttonSaveToFile.Location = new System.Drawing.Point(91, 13);
            this.buttonSaveToFile.Name = "buttonSaveToFile";
            this.buttonSaveToFile.Size = new System.Drawing.Size(93, 27);
            this.buttonSaveToFile.TabIndex = 11;
            this.buttonSaveToFile.Text = "ファイル書き出し";
            this.buttonSaveToFile.UseVisualStyleBackColor = true;
            this.buttonSaveToFile.Click += new System.EventHandler(this.buttonSaveToFile_Click);
            // 
            // buttonTODO
            // 
            this.buttonTODO.Location = new System.Drawing.Point(190, 13);
            this.buttonTODO.Name = "buttonTODO";
            this.buttonTODO.Size = new System.Drawing.Size(85, 27);
            this.buttonTODO.TabIndex = 12;
            this.buttonTODO.Text = "TODO出力";
            this.buttonTODO.UseVisualStyleBackColor = true;
            this.buttonTODO.Click += new System.EventHandler(this.buttonTODO_Click);
            // 
            // buttonFindSound
            // 
            this.buttonFindSound.Location = new System.Drawing.Point(281, 13);
            this.buttonFindSound.Name = "buttonFindSound";
            this.buttonFindSound.Size = new System.Drawing.Size(85, 27);
            this.buttonFindSound.TabIndex = 13;
            this.buttonFindSound.Text = "サウンド抽出";
            this.buttonFindSound.UseVisualStyleBackColor = true;
            this.buttonFindSound.Click += new System.EventHandler(this.buttonFindSound_Click);
            // 
            // buttonUpdateTable
            // 
            this.buttonUpdateTable.Location = new System.Drawing.Point(392, 13);
            this.buttonUpdateTable.Name = "buttonUpdateTable";
            this.buttonUpdateTable.Size = new System.Drawing.Size(146, 27);
            this.buttonUpdateTable.TabIndex = 14;
            this.buttonUpdateTable.Text = "テーブル更新(新しい方)";
            this.buttonUpdateTable.UseVisualStyleBackColor = true;
            this.buttonUpdateTable.Click += new System.EventHandler(this.buttonUpdateTable_Click);
            // 
            // buttonRemoteToLocal
            // 
            this.buttonRemoteToLocal.Location = new System.Drawing.Point(374, 46);
            this.buttonRemoteToLocal.Name = "buttonRemoteToLocal";
            this.buttonRemoteToLocal.Size = new System.Drawing.Size(83, 27);
            this.buttonRemoteToLocal.TabIndex = 15;
            this.buttonRemoteToLocal.Text = "テーブルpull";
            this.buttonRemoteToLocal.UseVisualStyleBackColor = true;
            this.buttonRemoteToLocal.Click += new System.EventHandler(this.buttonRemoteToLocal_Click);
            // 
            // buttonLocalToRemote
            // 
            this.buttonLocalToRemote.Location = new System.Drawing.Point(463, 46);
            this.buttonLocalToRemote.Name = "buttonLocalToRemote";
            this.buttonLocalToRemote.Size = new System.Drawing.Size(83, 27);
            this.buttonLocalToRemote.TabIndex = 16;
            this.buttonLocalToRemote.Text = "テーブルpush";
            this.buttonLocalToRemote.UseVisualStyleBackColor = true;
            this.buttonLocalToRemote.Click += new System.EventHandler(this.buttonLocalToRemote_Click);
            // 
            // buttonEventTag
            // 
            this.buttonEventTag.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.buttonEventTag.Location = new System.Drawing.Point(411, 400);
            this.buttonEventTag.Name = "buttonEventTag";
            this.buttonEventTag.Size = new System.Drawing.Size(81, 27);
            this.buttonEventTag.TabIndex = 17;
            this.buttonEventTag.Text = "イベントタグ";
            this.buttonEventTag.UseVisualStyleBackColor = true;
            this.buttonEventTag.Click += new System.EventHandler(this.buttonEventTag_Click);
            // 
            // EventList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(550, 487);
            this.Controls.Add(this.buttonEventTag);
            this.Controls.Add(this.buttonLocalToRemote);
            this.Controls.Add(this.buttonRemoteToLocal);
            this.Controls.Add(this.buttonUpdateTable);
            this.Controls.Add(this.buttonFindSound);
            this.Controls.Add(this.buttonTODO);
            this.Controls.Add(this.buttonSaveToFile);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonSkillList);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.buttonItemCreater);
            this.Controls.Add(this.buttonVariableCreater);
            this.Controls.Add(this.buttonFlagCreater);
            this.Controls.Add(this.buttonEventCreater);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBoxMainEvent);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EventList";
            this.Text = "イベントリスト";
            this.Load += new System.EventHandler(this.EventList_Load);
            this.EnabledChanged += new System.EventHandler(this.EventList_EnabledChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxMainEvent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonEventCreater;
        private System.Windows.Forms.Button buttonFlagCreater;
        private System.Windows.Forms.Button buttonVariableCreater;
        private System.Windows.Forms.Button buttonItemCreater;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button buttonSkillList;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button buttonSaveToFile;
        private System.Windows.Forms.Button buttonTODO;
        private System.Windows.Forms.Button buttonFindSound;
        private System.Windows.Forms.Button buttonUpdateTable;
        private System.Windows.Forms.Button buttonRemoteToLocal;
        private System.Windows.Forms.Button buttonLocalToRemote;
        private System.Windows.Forms.Button buttonEventTag;
    }
}

