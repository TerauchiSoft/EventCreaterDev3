﻿namespace EventCreater
{
    partial class VariableForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelVariavle = new System.Windows.Forms.Label();
            this.listBoxVariableBlocks = new System.Windows.Forms.ListBox();
            this.listBoxVariables = new System.Windows.Forms.ListBox();
            this.buttonMaxVariablesChange = new System.Windows.Forms.Button();
            this.textBoxVariableName = new System.Windows.Forms.TextBox();
            this.labelVariableNumber = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelVariavle
            // 
            this.labelVariavle.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelVariavle.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelVariavle.ForeColor = System.Drawing.SystemColors.Control;
            this.labelVariavle.Location = new System.Drawing.Point(23, 28);
            this.labelVariavle.Name = "labelVariavle";
            this.labelVariavle.Size = new System.Drawing.Size(133, 31);
            this.labelVariavle.TabIndex = 0;
            this.labelVariavle.Text = "変数";
            this.labelVariavle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBoxVariableBlocks
            // 
            this.listBoxVariableBlocks.FormattingEnabled = true;
            this.listBoxVariableBlocks.ItemHeight = 12;
            this.listBoxVariableBlocks.Items.AddRange(new object[] {
            "0001-0020:"});
            this.listBoxVariableBlocks.Location = new System.Drawing.Point(23, 71);
            this.listBoxVariableBlocks.Name = "listBoxVariableBlocks";
            this.listBoxVariableBlocks.ScrollAlwaysVisible = true;
            this.listBoxVariableBlocks.Size = new System.Drawing.Size(133, 268);
            this.listBoxVariableBlocks.TabIndex = 1;
            this.listBoxVariableBlocks.Click += new System.EventHandler(this.listBoxVariableBlocks_Click);
            this.listBoxVariableBlocks.SelectedValueChanged += new System.EventHandler(this.listBoxVariableBlocks_Click);
            // 
            // listBoxVariables
            // 
            this.listBoxVariables.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.listBoxVariables.FormattingEnabled = true;
            this.listBoxVariables.ItemHeight = 15;
            this.listBoxVariables.Items.AddRange(new object[] {
            "0001:",
            "0002:",
            "0003:",
            "0004:",
            "0005:",
            "0006:",
            "0007:",
            "0008:",
            "0009:",
            "0010:",
            "0011:",
            "0012:",
            "0013:",
            "0014:",
            "0015:",
            "0016:",
            "0017:",
            "0018:",
            "0019:",
            "0020:"});
            this.listBoxVariables.Location = new System.Drawing.Point(172, 28);
            this.listBoxVariables.Name = "listBoxVariables";
            this.listBoxVariables.Size = new System.Drawing.Size(224, 304);
            this.listBoxVariables.TabIndex = 2;
            this.listBoxVariables.Click += new System.EventHandler(this.listBoxVariables_Click);
            // 
            // buttonMaxVariablesChange
            // 
            this.buttonMaxVariablesChange.Location = new System.Drawing.Point(23, 354);
            this.buttonMaxVariablesChange.Name = "buttonMaxVariablesChange";
            this.buttonMaxVariablesChange.Size = new System.Drawing.Size(129, 29);
            this.buttonMaxVariablesChange.TabIndex = 3;
            this.buttonMaxVariablesChange.Text = "最大数の変更";
            this.buttonMaxVariablesChange.UseVisualStyleBackColor = true;
            this.buttonMaxVariablesChange.Click += new System.EventHandler(this.buttonMaxVariablesChange_Click);
            // 
            // textBoxVariableName
            // 
            this.textBoxVariableName.Location = new System.Drawing.Point(59, 20);
            this.textBoxVariableName.Name = "textBoxVariableName";
            this.textBoxVariableName.Size = new System.Drawing.Size(113, 19);
            this.textBoxVariableName.TabIndex = 4;
            this.textBoxVariableName.Enter += new System.EventHandler(this.textBoxVariableName_Enter);
            this.textBoxVariableName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxVariableName_KeyDown);
            this.textBoxVariableName.Leave += new System.EventHandler(this.textBoxVariableName_Leave);
            // 
            // labelVariableNumber
            // 
            this.labelVariableNumber.AutoSize = true;
            this.labelVariableNumber.Location = new System.Drawing.Point(15, 24);
            this.labelVariableNumber.Name = "labelVariableNumber";
            this.labelVariableNumber.Size = new System.Drawing.Size(26, 12);
            this.labelVariableNumber.TabIndex = 5;
            this.labelVariableNumber.Text = "num";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxVariableName);
            this.groupBox1.Controls.Add(this.labelVariableNumber);
            this.groupBox1.Location = new System.Drawing.Point(190, 342);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(192, 45);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "名前";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(172, 403);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 8;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // VariableForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 447);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonMaxVariablesChange);
            this.Controls.Add(this.listBoxVariables);
            this.Controls.Add(this.listBoxVariableBlocks);
            this.Controls.Add(this.labelVariavle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VariableForm";
            this.Text = "変数リスト";
            this.TopMost = true;
            this.EnabledChanged += new System.EventHandler(this.VariableForm_EnabledChanged);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelVariavle;
        private System.Windows.Forms.ListBox listBoxVariableBlocks;
        private System.Windows.Forms.ListBox listBoxVariables;
        private System.Windows.Forms.Button buttonMaxVariablesChange;
        private System.Windows.Forms.TextBox textBoxVariableName;
        private System.Windows.Forms.Label labelVariableNumber;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonOK;
    }
}