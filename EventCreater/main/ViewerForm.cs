﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EventCreater
{
    public partial class ViewerForm : Form
    {
        private EventList mainForm;
        public ViewerForm(IEnumerable<string> datas, EventList caller) {
            InitializeComponent();

            mainForm = caller;
            foreach (var str in datas) {
                viewer.Items.Add(str); 
            }
        }

        private void buttonOK_Click(object sender, EventArgs e) {
            this.Close();
        }

        /// <summary>
        /// 編集ビューを開く
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void viewer_DoubleClick(object sender, EventArgs e) {
            int idx = viewer.SelectedIndex;
            if (idx == -1)
                return;

            string str = viewer.Items[idx] as string;
            if (str.Contains("TODO") || str.Contains("PlaySE(") || str.Contains("PlayBGM("))
                idx--;

            var label = viewer.Items[idx] as string;
            label = label.Substring(0, 6);
            EventForm form = new EventForm(label);
            CloseAct.FormOpenCommon(mainForm, form);
        }

        /// <summary>
        /// テキストに出力
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOutPut_Click(object sender, EventArgs e) {
            // データのキャッシュ
            List<string> datas = new List<string>();
            foreach (var data in viewer.Items) {
                datas.Add(data.ToString());
            }

            // セーブダイアログ
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.FileName = "log.txt";
            sfd.InitialDirectory = AppPath.path; // バグってる
            sfd.Title = "抽出ログの書き出し";
            //ダイアログボックスを閉じる前に現在のディレクトリを復元するようにする
            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() == DialogResult.OK) {
                DirectoryUtils.SaveFile(sfd.FileName, datas);
            }
        }
    }
}
