﻿namespace EventCreater
{
    partial class EventForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelEventName = new System.Windows.Forms.Label();
            this.textBoxEventName = new System.Windows.Forms.TextBox();
            this.labelAppearanceCondition = new System.Windows.Forms.Label();
            this.checkBoxFlag = new System.Windows.Forms.CheckBox();
            this.labelFlagTrue = new System.Windows.Forms.Label();
            this.labelGa = new System.Windows.Forms.Label();
            this.checkBoxVariable = new System.Windows.Forms.CheckBox();
            this.numericUpDownVariable = new System.Windows.Forms.NumericUpDown();
            this.comboBoxVariableCondition = new System.Windows.Forms.ComboBox();
            this.checkBoxMember = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxMember = new System.Windows.Forms.ComboBox();
            this.EventContents = new System.Windows.Forms.ListBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.編集ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.挿入ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.コピーToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.切り取りToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.貼り付けToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.削除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OKButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.buttonFlagCreater = new System.Windows.Forms.Button();
            this.buttonVariableCreater = new System.Windows.Forms.Button();
            this.labelFlag = new System.Windows.Forms.Label();
            this.buttonFlag = new System.Windows.Forms.Button();
            this.buttonVariable = new System.Windows.Forms.Button();
            this.labelVariable = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.numericUpDownX = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownY = new System.Windows.Forms.NumericUpDown();
            this.labelPos = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelX = new System.Windows.Forms.Label();
            this.numericUpDownFloor = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.labelPlayCond = new System.Windows.Forms.Label();
            this.comboBoxPlayCond = new System.Windows.Forms.ComboBox();
            this.comboBoxFlagCondition = new System.Windows.Forms.ComboBox();
            this.comboBoxEventTag = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVariable)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFloor)).BeginInit();
            this.SuspendLayout();
            // 
            // labelEventName
            // 
            this.labelEventName.AutoSize = true;
            this.labelEventName.Location = new System.Drawing.Point(23, 22);
            this.labelEventName.Name = "labelEventName";
            this.labelEventName.Size = new System.Drawing.Size(53, 12);
            this.labelEventName.TabIndex = 0;
            this.labelEventName.Text = "イベント名";
            // 
            // textBoxEventName
            // 
            this.textBoxEventName.Location = new System.Drawing.Point(25, 46);
            this.textBoxEventName.Name = "textBoxEventName";
            this.textBoxEventName.Size = new System.Drawing.Size(214, 19);
            this.textBoxEventName.TabIndex = 1;
            this.textBoxEventName.TextChanged += new System.EventHandler(this.SetFormParamToCondition);
            this.textBoxEventName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxEventName_KeyDown);
            this.textBoxEventName.Leave += new System.EventHandler(this.SetFormParamToCondition);
            // 
            // labelAppearanceCondition
            // 
            this.labelAppearanceCondition.AutoSize = true;
            this.labelAppearanceCondition.Location = new System.Drawing.Point(23, 156);
            this.labelAppearanceCondition.Name = "labelAppearanceCondition";
            this.labelAppearanceCondition.Size = new System.Drawing.Size(89, 12);
            this.labelAppearanceCondition.TabIndex = 2;
            this.labelAppearanceCondition.Text = "イベント出現条件";
            // 
            // checkBoxFlag
            // 
            this.checkBoxFlag.AutoSize = true;
            this.checkBoxFlag.Location = new System.Drawing.Point(25, 180);
            this.checkBoxFlag.Name = "checkBoxFlag";
            this.checkBoxFlag.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBoxFlag.Size = new System.Drawing.Size(49, 16);
            this.checkBoxFlag.TabIndex = 3;
            this.checkBoxFlag.Text = "フラグ";
            this.checkBoxFlag.UseVisualStyleBackColor = true;
            this.checkBoxFlag.CheckedChanged += new System.EventHandler(this.checkBoxFlag_CheckedChanged);
            // 
            // labelFlagTrue
            // 
            this.labelFlagTrue.AutoSize = true;
            this.labelFlagTrue.Location = new System.Drawing.Point(180, 207);
            this.labelFlagTrue.Name = "labelFlagTrue";
            this.labelFlagTrue.Size = new System.Drawing.Size(15, 12);
            this.labelFlagTrue.TabIndex = 5;
            this.labelFlagTrue.Text = "が";
            // 
            // labelGa
            // 
            this.labelGa.AutoSize = true;
            this.labelGa.Location = new System.Drawing.Point(23, 296);
            this.labelGa.Name = "labelGa";
            this.labelGa.Size = new System.Drawing.Size(15, 12);
            this.labelGa.TabIndex = 8;
            this.labelGa.Text = "が";
            // 
            // checkBoxVariable
            // 
            this.checkBoxVariable.AutoSize = true;
            this.checkBoxVariable.Location = new System.Drawing.Point(25, 241);
            this.checkBoxVariable.Name = "checkBoxVariable";
            this.checkBoxVariable.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBoxVariable.Size = new System.Drawing.Size(48, 16);
            this.checkBoxVariable.TabIndex = 6;
            this.checkBoxVariable.Text = "変数";
            this.checkBoxVariable.UseVisualStyleBackColor = true;
            this.checkBoxVariable.CheckedChanged += new System.EventHandler(this.checkBoxVariable_CheckedChanged);
            // 
            // numericUpDownVariable
            // 
            this.numericUpDownVariable.Enabled = false;
            this.numericUpDownVariable.Location = new System.Drawing.Point(44, 292);
            this.numericUpDownVariable.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numericUpDownVariable.Minimum = new decimal(new int[] {
            999999,
            0,
            0,
            -2147483648});
            this.numericUpDownVariable.Name = "numericUpDownVariable";
            this.numericUpDownVariable.Size = new System.Drawing.Size(68, 19);
            this.numericUpDownVariable.TabIndex = 9;
            this.numericUpDownVariable.Enter += new System.EventHandler(this.SetFormParamToCondition);
            this.numericUpDownVariable.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxEventName_KeyDown);
            this.numericUpDownVariable.Leave += new System.EventHandler(this.SetFormParamToCondition);
            // 
            // comboBoxVariableCondition
            // 
            this.comboBoxVariableCondition.Enabled = false;
            this.comboBoxVariableCondition.FormattingEnabled = true;
            this.comboBoxVariableCondition.Items.AddRange(new object[] {
            "と同値",
            "以上",
            "以下",
            "より大きい",
            "より小さい"});
            this.comboBoxVariableCondition.Location = new System.Drawing.Point(132, 292);
            this.comboBoxVariableCondition.Name = "comboBoxVariableCondition";
            this.comboBoxVariableCondition.Size = new System.Drawing.Size(105, 20);
            this.comboBoxVariableCondition.TabIndex = 10;
            this.comboBoxVariableCondition.Enter += new System.EventHandler(this.SetFormParamToCondition);
            this.comboBoxVariableCondition.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxEventName_KeyDown);
            this.comboBoxVariableCondition.Leave += new System.EventHandler(this.SetFormParamToCondition);
            // 
            // checkBoxMember
            // 
            this.checkBoxMember.AutoSize = true;
            this.checkBoxMember.Location = new System.Drawing.Point(25, 324);
            this.checkBoxMember.Name = "checkBoxMember";
            this.checkBoxMember.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.checkBoxMember.Size = new System.Drawing.Size(61, 16);
            this.checkBoxMember.TabIndex = 11;
            this.checkBoxMember.Text = "メンバー";
            this.checkBoxMember.UseVisualStyleBackColor = true;
            this.checkBoxMember.CheckedChanged += new System.EventHandler(this.checkBoxMember_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(161, 349);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 12);
            this.label1.TabIndex = 13;
            this.label1.Text = "がいる。";
            // 
            // comboBoxMember
            // 
            this.comboBoxMember.Enabled = false;
            this.comboBoxMember.FormattingEnabled = true;
            this.comboBoxMember.Items.AddRange(new object[] {
            "アレスタ",
            "エリナ",
            "ペティル"});
            this.comboBoxMember.Location = new System.Drawing.Point(25, 346);
            this.comboBoxMember.Name = "comboBoxMember";
            this.comboBoxMember.Size = new System.Drawing.Size(121, 20);
            this.comboBoxMember.TabIndex = 12;
            this.comboBoxMember.Enter += new System.EventHandler(this.SetFormParamToCondition);
            this.comboBoxMember.Leave += new System.EventHandler(this.SetFormParamToCondition);
            // 
            // EventContents
            // 
            this.EventContents.FormattingEnabled = true;
            this.EventContents.ItemHeight = 12;
            this.EventContents.Location = new System.Drawing.Point(267, 32);
            this.EventContents.Name = "EventContents";
            this.EventContents.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.EventContents.Size = new System.Drawing.Size(588, 520);
            this.EventContents.TabIndex = 14;
            this.EventContents.DoubleClick += new System.EventHandler(this.EventContents_DoubleClick);
            this.EventContents.MouseDown += new System.Windows.Forms.MouseEventHandler(this.EventContents_MouseDown);
            this.EventContents.MouseUp += new System.Windows.Forms.MouseEventHandler(this.EventContents_MouseUp);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.編集ToolStripMenuItem,
            this.挿入ToolStripMenuItem,
            this.コピーToolStripMenuItem,
            this.切り取りToolStripMenuItem,
            this.貼り付けToolStripMenuItem,
            this.削除ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(116, 136);
            // 
            // 編集ToolStripMenuItem
            // 
            this.編集ToolStripMenuItem.Name = "編集ToolStripMenuItem";
            this.編集ToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.編集ToolStripMenuItem.Text = "編集";
            this.編集ToolStripMenuItem.Click += new System.EventHandler(this.編集ToolStripMenuItem_Click);
            // 
            // 挿入ToolStripMenuItem
            // 
            this.挿入ToolStripMenuItem.Name = "挿入ToolStripMenuItem";
            this.挿入ToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.挿入ToolStripMenuItem.Text = "挿入";
            this.挿入ToolStripMenuItem.Click += new System.EventHandler(this.挿入ToolStripMenuItem_Click);
            // 
            // コピーToolStripMenuItem
            // 
            this.コピーToolStripMenuItem.Name = "コピーToolStripMenuItem";
            this.コピーToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.コピーToolStripMenuItem.Text = "コピー";
            this.コピーToolStripMenuItem.Click += new System.EventHandler(this.コピーToolStripMenuItem_Click);
            // 
            // 切り取りToolStripMenuItem
            // 
            this.切り取りToolStripMenuItem.Name = "切り取りToolStripMenuItem";
            this.切り取りToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.切り取りToolStripMenuItem.Text = "切り取り";
            this.切り取りToolStripMenuItem.Click += new System.EventHandler(this.切り取りToolStripMenuItem_Click);
            // 
            // 貼り付けToolStripMenuItem
            // 
            this.貼り付けToolStripMenuItem.Name = "貼り付けToolStripMenuItem";
            this.貼り付けToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.貼り付けToolStripMenuItem.Text = "貼り付け";
            this.貼り付けToolStripMenuItem.Click += new System.EventHandler(this.貼り付けToolStripMenuItem_Click);
            // 
            // 削除ToolStripMenuItem
            // 
            this.削除ToolStripMenuItem.Name = "削除ToolStripMenuItem";
            this.削除ToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.削除ToolStripMenuItem.Text = "削除";
            this.削除ToolStripMenuItem.Click += new System.EventHandler(this.削除ToolStripMenuItem_Click);
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(335, 568);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 15;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(534, 568);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 16;
            this.CancelButton.Text = "キャンセル";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ApplyButton
            // 
            this.ApplyButton.Location = new System.Drawing.Point(736, 568);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 17;
            this.ApplyButton.Text = "適用";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // buttonFlagCreater
            // 
            this.buttonFlagCreater.Location = new System.Drawing.Point(23, 478);
            this.buttonFlagCreater.Name = "buttonFlagCreater";
            this.buttonFlagCreater.Size = new System.Drawing.Size(104, 26);
            this.buttonFlagCreater.TabIndex = 18;
            this.buttonFlagCreater.Text = "フラグリスト";
            this.buttonFlagCreater.UseVisualStyleBackColor = true;
            this.buttonFlagCreater.Click += new System.EventHandler(this.buttonFlagCreater_Click);
            // 
            // buttonVariableCreater
            // 
            this.buttonVariableCreater.Location = new System.Drawing.Point(23, 510);
            this.buttonVariableCreater.Name = "buttonVariableCreater";
            this.buttonVariableCreater.Size = new System.Drawing.Size(104, 26);
            this.buttonVariableCreater.TabIndex = 19;
            this.buttonVariableCreater.Text = "変数リスト";
            this.buttonVariableCreater.UseVisualStyleBackColor = true;
            this.buttonVariableCreater.Click += new System.EventHandler(this.buttonVariableCreater_Click);
            // 
            // labelFlag
            // 
            this.labelFlag.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelFlag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelFlag.Enabled = false;
            this.labelFlag.Location = new System.Drawing.Point(25, 204);
            this.labelFlag.Margin = new System.Windows.Forms.Padding(3);
            this.labelFlag.Name = "labelFlag";
            this.labelFlag.Size = new System.Drawing.Size(121, 18);
            this.labelFlag.TabIndex = 20;
            this.labelFlag.Text = "0001:";
            this.labelFlag.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelFlag.TextChanged += new System.EventHandler(this.SetFormParamToCondition);
            // 
            // buttonFlag
            // 
            this.buttonFlag.Enabled = false;
            this.buttonFlag.Location = new System.Drawing.Point(146, 203);
            this.buttonFlag.Name = "buttonFlag";
            this.buttonFlag.Size = new System.Drawing.Size(30, 20);
            this.buttonFlag.TabIndex = 21;
            this.buttonFlag.Text = "...";
            this.buttonFlag.UseVisualStyleBackColor = true;
            this.buttonFlag.Click += new System.EventHandler(this.buttonFlag_Click);
            // 
            // buttonVariable
            // 
            this.buttonVariable.Enabled = false;
            this.buttonVariable.Location = new System.Drawing.Point(146, 262);
            this.buttonVariable.Name = "buttonVariable";
            this.buttonVariable.Size = new System.Drawing.Size(30, 20);
            this.buttonVariable.TabIndex = 23;
            this.buttonVariable.Text = "...";
            this.buttonVariable.UseVisualStyleBackColor = true;
            this.buttonVariable.Click += new System.EventHandler(this.buttonVariable_Click);
            // 
            // labelVariable
            // 
            this.labelVariable.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelVariable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelVariable.Enabled = false;
            this.labelVariable.Location = new System.Drawing.Point(25, 263);
            this.labelVariable.Margin = new System.Windows.Forms.Padding(3);
            this.labelVariable.Name = "labelVariable";
            this.labelVariable.Size = new System.Drawing.Size(121, 18);
            this.labelVariable.TabIndex = 22;
            this.labelVariable.Text = "0001:";
            this.labelVariable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelVariable.TextChanged += new System.EventHandler(this.SetFormParamToCondition);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 20;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // numericUpDownX
            // 
            this.numericUpDownX.Location = new System.Drawing.Point(39, 453);
            this.numericUpDownX.Name = "numericUpDownX";
            this.numericUpDownX.Size = new System.Drawing.Size(59, 19);
            this.numericUpDownX.TabIndex = 24;
            // 
            // numericUpDownY
            // 
            this.numericUpDownY.Location = new System.Drawing.Point(182, 453);
            this.numericUpDownY.Name = "numericUpDownY";
            this.numericUpDownY.Size = new System.Drawing.Size(53, 19);
            this.numericUpDownY.TabIndex = 25;
            // 
            // labelPos
            // 
            this.labelPos.AutoSize = true;
            this.labelPos.Location = new System.Drawing.Point(21, 438);
            this.labelPos.Name = "labelPos";
            this.labelPos.Size = new System.Drawing.Size(113, 12);
            this.labelPos.TabIndex = 26;
            this.labelPos.Text = "イベント座標(左上が0)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(111, 455);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 27;
            this.label2.Text = "イベント座標";
            // 
            // labelX
            // 
            this.labelX.AutoSize = true;
            this.labelX.Location = new System.Drawing.Point(21, 455);
            this.labelX.Name = "labelX";
            this.labelX.Size = new System.Drawing.Size(12, 12);
            this.labelX.TabIndex = 28;
            this.labelX.Text = "X";
            // 
            // numericUpDownFloor
            // 
            this.numericUpDownFloor.Location = new System.Drawing.Point(39, 407);
            this.numericUpDownFloor.Name = "numericUpDownFloor";
            this.numericUpDownFloor.Size = new System.Drawing.Size(59, 19);
            this.numericUpDownFloor.TabIndex = 29;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 392);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(165, 12);
            this.label3.TabIndex = 30;
            this.label3.Text = "イベントフロア(0でコモンイベント化)";
            // 
            // labelPlayCond
            // 
            this.labelPlayCond.AutoSize = true;
            this.labelPlayCond.Location = new System.Drawing.Point(23, 96);
            this.labelPlayCond.Name = "labelPlayCond";
            this.labelPlayCond.Size = new System.Drawing.Size(89, 12);
            this.labelPlayCond.TabIndex = 31;
            this.labelPlayCond.Text = "イベント実行条件";
            // 
            // comboBoxPlayCond
            // 
            this.comboBoxPlayCond.FormattingEnabled = true;
            this.comboBoxPlayCond.Items.AddRange(new object[] {
            "調べて実行",
            "接触して実行",
            "自動的に実行"});
            this.comboBoxPlayCond.Location = new System.Drawing.Point(22, 111);
            this.comboBoxPlayCond.Name = "comboBoxPlayCond";
            this.comboBoxPlayCond.Size = new System.Drawing.Size(105, 20);
            this.comboBoxPlayCond.TabIndex = 32;
            // 
            // comboBoxFlagCondition
            // 
            this.comboBoxFlagCondition.Enabled = false;
            this.comboBoxFlagCondition.FormattingEnabled = true;
            this.comboBoxFlagCondition.Items.AddRange(new object[] {
            "false",
            "true"});
            this.comboBoxFlagCondition.Location = new System.Drawing.Point(201, 204);
            this.comboBoxFlagCondition.Name = "comboBoxFlagCondition";
            this.comboBoxFlagCondition.Size = new System.Drawing.Size(54, 20);
            this.comboBoxFlagCondition.TabIndex = 33;
            // 
            // comboBoxEventTag
            // 
            this.comboBoxEventTag.FormattingEnabled = true;
            this.comboBoxEventTag.Items.AddRange(new object[] {
            "調べて実行",
            "接触して実行",
            "自動的に実行"});
            this.comboBoxEventTag.Location = new System.Drawing.Point(143, 111);
            this.comboBoxEventTag.Name = "comboBoxEventTag";
            this.comboBoxEventTag.Size = new System.Drawing.Size(105, 20);
            this.comboBoxEventTag.TabIndex = 35;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(144, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 12);
            this.label4.TabIndex = 34;
            this.label4.Text = "イベントタグ";
            // 
            // EventForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(883, 599);
            this.Controls.Add(this.comboBoxEventTag);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBoxFlagCondition);
            this.Controls.Add(this.comboBoxPlayCond);
            this.Controls.Add(this.labelPlayCond);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numericUpDownFloor);
            this.Controls.Add(this.labelX);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelPos);
            this.Controls.Add(this.numericUpDownY);
            this.Controls.Add(this.numericUpDownX);
            this.Controls.Add(this.buttonVariable);
            this.Controls.Add(this.labelVariable);
            this.Controls.Add(this.buttonFlag);
            this.Controls.Add(this.labelFlag);
            this.Controls.Add(this.buttonVariableCreater);
            this.Controls.Add(this.buttonFlagCreater);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.EventContents);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxMember);
            this.Controls.Add(this.checkBoxMember);
            this.Controls.Add(this.comboBoxVariableCondition);
            this.Controls.Add(this.numericUpDownVariable);
            this.Controls.Add(this.labelGa);
            this.Controls.Add(this.checkBoxVariable);
            this.Controls.Add(this.labelFlagTrue);
            this.Controls.Add(this.checkBoxFlag);
            this.Controls.Add(this.labelAppearanceCondition);
            this.Controls.Add(this.textBoxEventName);
            this.Controls.Add(this.labelEventName);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EventForm";
            this.Text = "イベント作成";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EventForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVariable)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFloor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelEventName;
        private System.Windows.Forms.TextBox textBoxEventName;
        private System.Windows.Forms.Label labelAppearanceCondition;
        private System.Windows.Forms.CheckBox checkBoxFlag;
        private System.Windows.Forms.Label labelFlagTrue;
        private System.Windows.Forms.Label labelGa;
        private System.Windows.Forms.CheckBox checkBoxVariable;
        private System.Windows.Forms.NumericUpDown numericUpDownVariable;
        private System.Windows.Forms.ComboBox comboBoxVariableCondition;
        private System.Windows.Forms.CheckBox checkBoxMember;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxMember;
        private System.Windows.Forms.Button OKButton;
        private new System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button buttonFlagCreater;
        private System.Windows.Forms.Button buttonVariableCreater;
        private System.Windows.Forms.Label labelFlag;
        private System.Windows.Forms.Button buttonFlag;
        private System.Windows.Forms.Button buttonVariable;
        private System.Windows.Forms.Label labelVariable;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 編集ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 挿入ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 削除ToolStripMenuItem;
        public System.Windows.Forms.ListBox EventContents;
        private System.Windows.Forms.ToolStripMenuItem コピーToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 切り取りToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 貼り付けToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.NumericUpDown numericUpDownX;
        private System.Windows.Forms.NumericUpDown numericUpDownY;
        private System.Windows.Forms.Label labelPos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelX;
        private System.Windows.Forms.NumericUpDown numericUpDownFloor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelPlayCond;
        private System.Windows.Forms.ComboBox comboBoxPlayCond;
        private System.Windows.Forms.ComboBox comboBoxFlagCondition;
        private System.Windows.Forms.ComboBox comboBoxEventTag;
        private System.Windows.Forms.Label label4;
    }
}