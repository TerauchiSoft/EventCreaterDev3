﻿namespace EventCreater
{
    partial class EventActListForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonBack = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.PlaceMove = new System.Windows.Forms.Button();
            this.StatusChange = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.buttonLv = new System.Windows.Forms.Button();
            this.buttonMen = new System.Windows.Forms.Button();
            this.buttonItem = new System.Windows.Forms.Button();
            this.buttonMoney = new System.Windows.Forms.Button();
            this.buttonVariable = new System.Windows.Forms.Button();
            this.buttonFlag = new System.Windows.Forms.Button();
            this.buttonCareAll = new System.Windows.Forms.Button();
            this.buttonHpMp = new System.Windows.Forms.Button();
            this.buttonSkill = new System.Windows.Forms.Button();
            this.buttonExp = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.buttonBGMStop = new System.Windows.Forms.Button();
            this.buttonSE = new System.Windows.Forms.Button();
            this.buttonBGM = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonEffect = new System.Windows.Forms.Button();
            this.buttonShake = new System.Windows.Forms.Button();
            this.buttonFlash = new System.Windows.Forms.Button();
            this.buttonColorTone = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.buttonCharaBack = new System.Windows.Forms.Button();
            this.buttonCharaFront = new System.Windows.Forms.Button();
            this.buttonWait = new System.Windows.Forms.Button();
            this.GotoLabel = new System.Windows.Forms.Button();
            this.SetLabel = new System.Windows.Forms.Button();
            this.NoWriteLine = new System.Windows.Forms.Button();
            this.buttonCharaname = new System.Windows.Forms.Button();
            this.buttonCharadel = new System.Windows.Forms.Button();
            this.buttonBranch = new System.Windows.Forms.Button();
            this.buttonNote = new System.Windows.Forms.Button();
            this.buttonChoice = new System.Windows.Forms.Button();
            this.buttonChara = new System.Windows.Forms.Button();
            this.buttonText = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.EventDelete = new System.Windows.Forms.Button();
            this.Break = new System.Windows.Forms.Button();
            this.Camp = new System.Windows.Forms.Button();
            this.Battle = new System.Windows.Forms.Button();
            this.CallEvent = new System.Windows.Forms.Button();
            this.Shop = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.InputText = new System.Windows.Forms.Button();
            this.GameMethod = new System.Windows.Forms.Button();
            this.NumInput = new System.Windows.Forms.Button();
            this.tabPage3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonBack
            // 
            this.buttonBack.Location = new System.Drawing.Point(284, 411);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(75, 23);
            this.buttonBack.TabIndex = 1;
            this.buttonBack.Text = "戻る";
            this.buttonBack.UseVisualStyleBackColor = true;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Transparent;
            this.tabPage3.Controls.Add(this.PlaceMove);
            this.tabPage3.Controls.Add(this.StatusChange);
            this.tabPage3.Controls.Add(this.button4);
            this.tabPage3.Controls.Add(this.button2);
            this.tabPage3.Controls.Add(this.buttonLv);
            this.tabPage3.Controls.Add(this.buttonMen);
            this.tabPage3.Controls.Add(this.buttonItem);
            this.tabPage3.Controls.Add(this.buttonMoney);
            this.tabPage3.Controls.Add(this.buttonVariable);
            this.tabPage3.Controls.Add(this.buttonFlag);
            this.tabPage3.Controls.Add(this.buttonCareAll);
            this.tabPage3.Controls.Add(this.buttonHpMp);
            this.tabPage3.Controls.Add(this.buttonSkill);
            this.tabPage3.Controls.Add(this.buttonExp);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(364, 353);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "内部データ関係";
            // 
            // PlaceMove
            // 
            this.PlaceMove.Location = new System.Drawing.Point(200, 201);
            this.PlaceMove.Name = "PlaceMove";
            this.PlaceMove.Size = new System.Drawing.Size(158, 31);
            this.PlaceMove.TabIndex = 26;
            this.PlaceMove.Text = "場所移動";
            this.PlaceMove.UseVisualStyleBackColor = true;
            this.PlaceMove.Click += new System.EventHandler(this.PlaceMove_Click);
            // 
            // StatusChange
            // 
            this.StatusChange.Location = new System.Drawing.Point(16, 201);
            this.StatusChange.Name = "StatusChange";
            this.StatusChange.Size = new System.Drawing.Size(158, 31);
            this.StatusChange.TabIndex = 25;
            this.StatusChange.Text = "状態の変更";
            this.StatusChange.UseVisualStyleBackColor = true;
            this.StatusChange.Click += new System.EventHandler(this.StatusChange_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(200, 279);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(158, 31);
            this.button4.TabIndex = 24;
            this.button4.Text = "空白行";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.NoWriteLine_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(200, 316);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(158, 31);
            this.button2.TabIndex = 23;
            this.button2.Text = "注釈、メモ(ゲームには無影響)";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.buttonNote_Click);
            // 
            // buttonLv
            // 
            this.buttonLv.Location = new System.Drawing.Point(16, 53);
            this.buttonLv.Name = "buttonLv";
            this.buttonLv.Size = new System.Drawing.Size(158, 31);
            this.buttonLv.TabIndex = 22;
            this.buttonLv.Text = "レベルの増減";
            this.buttonLv.UseVisualStyleBackColor = true;
            this.buttonLv.Click += new System.EventHandler(this.buttonLv_Click);
            // 
            // buttonMen
            // 
            this.buttonMen.Location = new System.Drawing.Point(200, 90);
            this.buttonMen.Name = "buttonMen";
            this.buttonMen.Size = new System.Drawing.Size(158, 31);
            this.buttonMen.TabIndex = 21;
            this.buttonMen.Text = "メンバーの入れ替え";
            this.buttonMen.UseVisualStyleBackColor = true;
            this.buttonMen.Click += new System.EventHandler(this.buttonMen_Click);
            // 
            // buttonItem
            // 
            this.buttonItem.Location = new System.Drawing.Point(200, 127);
            this.buttonItem.Name = "buttonItem";
            this.buttonItem.Size = new System.Drawing.Size(158, 31);
            this.buttonItem.TabIndex = 20;
            this.buttonItem.Text = "アイテムの操作";
            this.buttonItem.UseVisualStyleBackColor = true;
            this.buttonItem.Click += new System.EventHandler(this.buttonItem_Click);
            // 
            // buttonMoney
            // 
            this.buttonMoney.Location = new System.Drawing.Point(16, 164);
            this.buttonMoney.Name = "buttonMoney";
            this.buttonMoney.Size = new System.Drawing.Size(158, 31);
            this.buttonMoney.TabIndex = 19;
            this.buttonMoney.Text = "所持金の操作";
            this.buttonMoney.UseVisualStyleBackColor = true;
            this.buttonMoney.Click += new System.EventHandler(this.buttonMoney_Click);
            // 
            // buttonVariable
            // 
            this.buttonVariable.Location = new System.Drawing.Point(200, 53);
            this.buttonVariable.Name = "buttonVariable";
            this.buttonVariable.Size = new System.Drawing.Size(158, 31);
            this.buttonVariable.TabIndex = 18;
            this.buttonVariable.Text = "変数の操作";
            this.buttonVariable.UseVisualStyleBackColor = true;
            this.buttonVariable.Click += new System.EventHandler(this.buttonVariable_Click);
            // 
            // buttonFlag
            // 
            this.buttonFlag.Location = new System.Drawing.Point(200, 16);
            this.buttonFlag.Name = "buttonFlag";
            this.buttonFlag.Size = new System.Drawing.Size(158, 31);
            this.buttonFlag.TabIndex = 17;
            this.buttonFlag.Text = "フラグの操作";
            this.buttonFlag.UseVisualStyleBackColor = true;
            this.buttonFlag.Click += new System.EventHandler(this.buttonFlag_Click);
            // 
            // buttonCareAll
            // 
            this.buttonCareAll.Location = new System.Drawing.Point(16, 127);
            this.buttonCareAll.Name = "buttonCareAll";
            this.buttonCareAll.Size = new System.Drawing.Size(158, 31);
            this.buttonCareAll.TabIndex = 16;
            this.buttonCareAll.Text = "全回復";
            this.buttonCareAll.UseVisualStyleBackColor = true;
            this.buttonCareAll.Click += new System.EventHandler(this.buttonCareAll_Click);
            // 
            // buttonHpMp
            // 
            this.buttonHpMp.Location = new System.Drawing.Point(16, 90);
            this.buttonHpMp.Name = "buttonHpMp";
            this.buttonHpMp.Size = new System.Drawing.Size(158, 31);
            this.buttonHpMp.TabIndex = 15;
            this.buttonHpMp.Text = "HP,MPの変更";
            this.buttonHpMp.UseVisualStyleBackColor = true;
            this.buttonHpMp.Click += new System.EventHandler(this.buttonHpMp_Click);
            // 
            // buttonSkill
            // 
            this.buttonSkill.Location = new System.Drawing.Point(200, 164);
            this.buttonSkill.Name = "buttonSkill";
            this.buttonSkill.Size = new System.Drawing.Size(158, 31);
            this.buttonSkill.TabIndex = 14;
            this.buttonSkill.Text = "スキルの増減";
            this.buttonSkill.UseVisualStyleBackColor = true;
            this.buttonSkill.Click += new System.EventHandler(this.buttonSkill_Click);
            // 
            // buttonExp
            // 
            this.buttonExp.Location = new System.Drawing.Point(16, 16);
            this.buttonExp.Name = "buttonExp";
            this.buttonExp.Size = new System.Drawing.Size(158, 31);
            this.buttonExp.TabIndex = 13;
            this.buttonExp.Text = "経験値の増減";
            this.buttonExp.UseVisualStyleBackColor = true;
            this.buttonExp.Click += new System.EventHandler(this.buttonExp_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Transparent;
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.buttonBGMStop);
            this.tabPage2.Controls.Add(this.buttonSE);
            this.tabPage2.Controls.Add(this.buttonBGM);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.buttonEffect);
            this.tabPage2.Controls.Add(this.buttonShake);
            this.tabPage2.Controls.Add(this.buttonFlash);
            this.tabPage2.Controls.Add(this.buttonColorTone);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(364, 353);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "音、画像関係";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(200, 279);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(158, 31);
            this.button3.TabIndex = 25;
            this.button3.Text = "空白行";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.NoWriteLine_Click);
            // 
            // buttonBGMStop
            // 
            this.buttonBGMStop.Location = new System.Drawing.Point(190, 90);
            this.buttonBGMStop.Name = "buttonBGMStop";
            this.buttonBGMStop.Size = new System.Drawing.Size(158, 31);
            this.buttonBGMStop.TabIndex = 24;
            this.buttonBGMStop.Text = "BGM停止";
            this.buttonBGMStop.UseVisualStyleBackColor = true;
            this.buttonBGMStop.Click += new System.EventHandler(this.buttonBGMStop_Click);
            // 
            // buttonSE
            // 
            this.buttonSE.Location = new System.Drawing.Point(190, 53);
            this.buttonSE.Name = "buttonSE";
            this.buttonSE.Size = new System.Drawing.Size(158, 31);
            this.buttonSE.TabIndex = 23;
            this.buttonSE.Text = "効果音の再生";
            this.buttonSE.UseVisualStyleBackColor = true;
            this.buttonSE.Click += new System.EventHandler(this.buttonSE_Click);
            // 
            // buttonBGM
            // 
            this.buttonBGM.Location = new System.Drawing.Point(190, 16);
            this.buttonBGM.Name = "buttonBGM";
            this.buttonBGM.Size = new System.Drawing.Size(158, 31);
            this.buttonBGM.TabIndex = 22;
            this.buttonBGM.Text = "BGMの再生";
            this.buttonBGM.UseVisualStyleBackColor = true;
            this.buttonBGM.Click += new System.EventHandler(this.buttonBGM_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(200, 316);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(158, 31);
            this.button1.TabIndex = 21;
            this.button1.Text = "注釈、メモ(ゲームには無影響)";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.buttonNote_Click);
            // 
            // buttonEffect
            // 
            this.buttonEffect.Location = new System.Drawing.Point(16, 127);
            this.buttonEffect.Name = "buttonEffect";
            this.buttonEffect.Size = new System.Drawing.Size(158, 31);
            this.buttonEffect.TabIndex = 20;
            this.buttonEffect.Text = "エフェクト再生";
            this.buttonEffect.UseVisualStyleBackColor = true;
            this.buttonEffect.Click += new System.EventHandler(this.buttonEffect_Click);
            // 
            // buttonShake
            // 
            this.buttonShake.Location = new System.Drawing.Point(16, 90);
            this.buttonShake.Name = "buttonShake";
            this.buttonShake.Size = new System.Drawing.Size(158, 31);
            this.buttonShake.TabIndex = 19;
            this.buttonShake.Text = "画面のシェイク";
            this.buttonShake.UseVisualStyleBackColor = true;
            this.buttonShake.Click += new System.EventHandler(this.buttonShake_Click);
            // 
            // buttonFlash
            // 
            this.buttonFlash.Location = new System.Drawing.Point(16, 53);
            this.buttonFlash.Name = "buttonFlash";
            this.buttonFlash.Size = new System.Drawing.Size(158, 31);
            this.buttonFlash.TabIndex = 18;
            this.buttonFlash.Text = "画面のフラッシュ";
            this.buttonFlash.UseVisualStyleBackColor = true;
            this.buttonFlash.Click += new System.EventHandler(this.buttonFlash_Click);
            // 
            // buttonColorTone
            // 
            this.buttonColorTone.Location = new System.Drawing.Point(16, 16);
            this.buttonColorTone.Name = "buttonColorTone";
            this.buttonColorTone.Size = new System.Drawing.Size(158, 31);
            this.buttonColorTone.TabIndex = 17;
            this.buttonColorTone.Text = "画面の色調変更";
            this.buttonColorTone.UseVisualStyleBackColor = true;
            this.buttonColorTone.Click += new System.EventHandler(this.buttonColorTone_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Transparent;
            this.tabPage1.Controls.Add(this.buttonCharaBack);
            this.tabPage1.Controls.Add(this.buttonCharaFront);
            this.tabPage1.Controls.Add(this.buttonWait);
            this.tabPage1.Controls.Add(this.GotoLabel);
            this.tabPage1.Controls.Add(this.SetLabel);
            this.tabPage1.Controls.Add(this.NoWriteLine);
            this.tabPage1.Controls.Add(this.buttonCharaname);
            this.tabPage1.Controls.Add(this.buttonCharadel);
            this.tabPage1.Controls.Add(this.buttonBranch);
            this.tabPage1.Controls.Add(this.buttonNote);
            this.tabPage1.Controls.Add(this.buttonChoice);
            this.tabPage1.Controls.Add(this.buttonChara);
            this.tabPage1.Controls.Add(this.buttonText);
            this.tabPage1.Controls.Add(this.NumInput);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(364, 353);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "イベント進行関係";
            // 
            // buttonCharaBack
            // 
            this.buttonCharaBack.Location = new System.Drawing.Point(190, 90);
            this.buttonCharaBack.Name = "buttonCharaBack";
            this.buttonCharaBack.Size = new System.Drawing.Size(158, 31);
            this.buttonCharaBack.TabIndex = 27;
            this.buttonCharaBack.Text = "キャラを最背面に移動";
            this.buttonCharaBack.UseVisualStyleBackColor = true;
            this.buttonCharaBack.Click += new System.EventHandler(this.CharaBack_Click);
            // 
            // buttonCharaFront
            // 
            this.buttonCharaFront.Location = new System.Drawing.Point(16, 90);
            this.buttonCharaFront.Name = "buttonCharaFront";
            this.buttonCharaFront.Size = new System.Drawing.Size(158, 31);
            this.buttonCharaFront.TabIndex = 26;
            this.buttonCharaFront.Text = "キャラを最前面に移動";
            this.buttonCharaFront.UseVisualStyleBackColor = true;
            this.buttonCharaFront.Click += new System.EventHandler(this.CharaFront_Click);
            // 
            // buttonWait
            // 
            this.buttonWait.Location = new System.Drawing.Point(190, 201);
            this.buttonWait.Name = "buttonWait";
            this.buttonWait.Size = new System.Drawing.Size(158, 31);
            this.buttonWait.TabIndex = 25;
            this.buttonWait.Text = "ウェイト";
            this.buttonWait.UseVisualStyleBackColor = true;
            this.buttonWait.Click += new System.EventHandler(this.Wait_Click);
            // 
            // GotoLabel
            // 
            this.GotoLabel.Location = new System.Drawing.Point(190, 164);
            this.GotoLabel.Name = "GotoLabel";
            this.GotoLabel.Size = new System.Drawing.Size(158, 31);
            this.GotoLabel.TabIndex = 24;
            this.GotoLabel.Text = "指定のラベルに飛ぶ";
            this.GotoLabel.UseVisualStyleBackColor = true;
            this.GotoLabel.Click += new System.EventHandler(this.GotoLabel_Click);
            // 
            // SetLabel
            // 
            this.SetLabel.Location = new System.Drawing.Point(16, 164);
            this.SetLabel.Name = "SetLabel";
            this.SetLabel.Size = new System.Drawing.Size(158, 31);
            this.SetLabel.TabIndex = 23;
            this.SetLabel.Text = "ラベルの設置";
            this.SetLabel.UseVisualStyleBackColor = true;
            this.SetLabel.Click += new System.EventHandler(this.SetLabel_Click);
            // 
            // NoWriteLine
            // 
            this.NoWriteLine.Location = new System.Drawing.Point(200, 279);
            this.NoWriteLine.Name = "NoWriteLine";
            this.NoWriteLine.Size = new System.Drawing.Size(158, 31);
            this.NoWriteLine.TabIndex = 21;
            this.NoWriteLine.Text = "空白行";
            this.NoWriteLine.UseVisualStyleBackColor = true;
            this.NoWriteLine.Click += new System.EventHandler(this.NoWriteLine_Click);
            // 
            // buttonCharaname
            // 
            this.buttonCharaname.Location = new System.Drawing.Point(190, 16);
            this.buttonCharaname.Name = "buttonCharaname";
            this.buttonCharaname.Size = new System.Drawing.Size(158, 31);
            this.buttonCharaname.TabIndex = 20;
            this.buttonCharaname.Text = "キャラ名の表示";
            this.buttonCharaname.UseVisualStyleBackColor = true;
            this.buttonCharaname.Click += new System.EventHandler(this.buttonCharaname_Click);
            // 
            // buttonCharadel
            // 
            this.buttonCharadel.Location = new System.Drawing.Point(190, 53);
            this.buttonCharadel.Name = "buttonCharadel";
            this.buttonCharadel.Size = new System.Drawing.Size(158, 31);
            this.buttonCharadel.TabIndex = 19;
            this.buttonCharadel.Text = "キャラクターの消去";
            this.buttonCharadel.UseVisualStyleBackColor = true;
            this.buttonCharadel.Click += new System.EventHandler(this.buttonCharadel_Click);
            // 
            // buttonBranch
            // 
            this.buttonBranch.Location = new System.Drawing.Point(16, 201);
            this.buttonBranch.Name = "buttonBranch";
            this.buttonBranch.Size = new System.Drawing.Size(158, 31);
            this.buttonBranch.TabIndex = 18;
            this.buttonBranch.Text = "条件分岐";
            this.buttonBranch.UseVisualStyleBackColor = true;
            this.buttonBranch.Click += new System.EventHandler(this.buttonBranch_Click);
            // 
            // buttonNote
            // 
            this.buttonNote.Location = new System.Drawing.Point(200, 316);
            this.buttonNote.Name = "buttonNote";
            this.buttonNote.Size = new System.Drawing.Size(158, 31);
            this.buttonNote.TabIndex = 17;
            this.buttonNote.Text = "注釈、メモ(ゲームには無影響)";
            this.buttonNote.UseVisualStyleBackColor = true;
            this.buttonNote.Click += new System.EventHandler(this.buttonNote_Click);
            // 
            // buttonChoice
            // 
            this.buttonChoice.Location = new System.Drawing.Point(16, 127);
            this.buttonChoice.Name = "buttonChoice";
            this.buttonChoice.Size = new System.Drawing.Size(158, 31);
            this.buttonChoice.TabIndex = 2;
            this.buttonChoice.Text = "選択肢の表示";
            this.buttonChoice.UseVisualStyleBackColor = true;
            this.buttonChoice.Click += new System.EventHandler(this.buttonChoice_Click);
            // 
            // buttonChara
            // 
            this.buttonChara.Location = new System.Drawing.Point(16, 53);
            this.buttonChara.Name = "buttonChara";
            this.buttonChara.Size = new System.Drawing.Size(158, 31);
            this.buttonChara.TabIndex = 1;
            this.buttonChara.Text = "キャラクターの表示";
            this.buttonChara.UseVisualStyleBackColor = true;
            this.buttonChara.Click += new System.EventHandler(this.buttonChara_Click);
            // 
            // buttonText
            // 
            this.buttonText.Location = new System.Drawing.Point(16, 16);
            this.buttonText.Name = "buttonText";
            this.buttonText.Size = new System.Drawing.Size(158, 31);
            this.buttonText.TabIndex = 0;
            this.buttonText.Text = "文章の表示";
            this.buttonText.UseVisualStyleBackColor = true;
            this.buttonText.Click += new System.EventHandler(this.buttonText_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(29, 13);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(372, 379);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.Transparent;
            this.tabPage4.Controls.Add(this.GameMethod);
            this.tabPage4.Controls.Add(this.InputText);
            this.tabPage4.Controls.Add(this.EventDelete);
            this.tabPage4.Controls.Add(this.Break);
            this.tabPage4.Controls.Add(this.Camp);
            this.tabPage4.Controls.Add(this.Battle);
            this.tabPage4.Controls.Add(this.CallEvent);
            this.tabPage4.Controls.Add(this.Shop);
            this.tabPage4.Controls.Add(this.button6);
            this.tabPage4.Controls.Add(this.button5);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(364, 353);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "コモン";
            // 
            // EventDelete
            // 
            this.EventDelete.Location = new System.Drawing.Point(200, 90);
            this.EventDelete.Name = "EventDelete";
            this.EventDelete.Size = new System.Drawing.Size(158, 31);
            this.EventDelete.TabIndex = 30;
            this.EventDelete.Text = "イベントの一時削除";
            this.EventDelete.UseVisualStyleBackColor = true;
            this.EventDelete.Click += new System.EventHandler(this.EventDelete_Click);
            // 
            // Break
            // 
            this.Break.Location = new System.Drawing.Point(16, 90);
            this.Break.Name = "Break";
            this.Break.Size = new System.Drawing.Size(158, 31);
            this.Break.TabIndex = 29;
            this.Break.Text = "イベント処理の中断";
            this.Break.UseVisualStyleBackColor = true;
            this.Break.Click += new System.EventHandler(this.Break_Click);
            // 
            // Camp
            // 
            this.Camp.Location = new System.Drawing.Point(200, 53);
            this.Camp.Name = "Camp";
            this.Camp.Size = new System.Drawing.Size(158, 31);
            this.Camp.TabIndex = 28;
            this.Camp.Text = "拠点の処理";
            this.Camp.UseVisualStyleBackColor = true;
            this.Camp.Click += new System.EventHandler(this.Camp_Click);
            // 
            // Battle
            // 
            this.Battle.Location = new System.Drawing.Point(16, 53);
            this.Battle.Name = "Battle";
            this.Battle.Size = new System.Drawing.Size(158, 31);
            this.Battle.TabIndex = 27;
            this.Battle.Text = "戦闘の処理";
            this.Battle.UseVisualStyleBackColor = true;
            this.Battle.Click += new System.EventHandler(this.Battle_Click);
            // 
            // CallEvent
            // 
            this.CallEvent.Location = new System.Drawing.Point(200, 16);
            this.CallEvent.Name = "CallEvent";
            this.CallEvent.Size = new System.Drawing.Size(158, 31);
            this.CallEvent.TabIndex = 26;
            this.CallEvent.Text = "他イベントの呼び出し";
            this.CallEvent.UseVisualStyleBackColor = true;
            this.CallEvent.Click += new System.EventHandler(this.CallEvent_Click);
            // 
            // Shop
            // 
            this.Shop.Location = new System.Drawing.Point(16, 16);
            this.Shop.Name = "Shop";
            this.Shop.Size = new System.Drawing.Size(158, 31);
            this.Shop.TabIndex = 25;
            this.Shop.Text = "お店の処理";
            this.Shop.UseVisualStyleBackColor = true;
            this.Shop.Click += new System.EventHandler(this.Shop_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(200, 316);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(158, 31);
            this.button6.TabIndex = 24;
            this.button6.Text = "注釈、メモ(ゲームには無影響)";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.buttonNote_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(200, 279);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(158, 31);
            this.button5.TabIndex = 22;
            this.button5.Text = "空白行";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.NoWriteLine_Click);
            // 
            // InputText
            // 
            this.InputText.Location = new System.Drawing.Point(16, 127);
            this.InputText.Name = "InputText";
            this.InputText.Size = new System.Drawing.Size(158, 31);
            this.InputText.TabIndex = 31;
            this.InputText.Text = "テキストの入力";
            this.InputText.UseVisualStyleBackColor = true;
            this.InputText.Click += new System.EventHandler(this.InputText_Click);
            // 
            // GameMethod
            // 
            this.GameMethod.Location = new System.Drawing.Point(200, 127);
            this.GameMethod.Name = "GameMethod";
            this.GameMethod.Size = new System.Drawing.Size(158, 31);
            this.GameMethod.TabIndex = 32;
            this.GameMethod.Text = "ゲーム内メソッドを呼び出す";
            this.GameMethod.UseVisualStyleBackColor = true;
            this.GameMethod.Click += new System.EventHandler(this.GameMethod_Click);
            // 
            // NumInput
            // 
            this.NumInput.Location = new System.Drawing.Point(190, 127);
            this.NumInput.Name = "NumInput";
            this.NumInput.Size = new System.Drawing.Size(158, 31);
            this.NumInput.TabIndex = 22;
            this.NumInput.Text = "数値入力の処理";
            this.NumInput.UseVisualStyleBackColor = true;
            this.NumInput.Click += new System.EventHandler(this.NumInput_Click);
            // 
            // EventActListForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ClientSize = new System.Drawing.Size(433, 455);
            this.Controls.Add(this.buttonBack);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EventActListForm";
            this.Text = "イベントコマンド";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EventActListForm_FormClosed);
            this.tabPage3.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonBack;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button buttonLv;
        private System.Windows.Forms.Button buttonMen;
        private System.Windows.Forms.Button buttonItem;
        private System.Windows.Forms.Button buttonMoney;
        private System.Windows.Forms.Button buttonVariable;
        private System.Windows.Forms.Button buttonFlag;
        private System.Windows.Forms.Button buttonCareAll;
        private System.Windows.Forms.Button buttonHpMp;
        private System.Windows.Forms.Button buttonSkill;
        private System.Windows.Forms.Button buttonExp;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonEffect;
        private System.Windows.Forms.Button buttonShake;
        private System.Windows.Forms.Button buttonFlash;
        private System.Windows.Forms.Button buttonColorTone;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button buttonNote;
        private System.Windows.Forms.Button buttonChoice;
        private System.Windows.Forms.Button buttonChara;
        private System.Windows.Forms.Button buttonText;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button buttonBranch;
        private System.Windows.Forms.Button buttonCharadel;
        private System.Windows.Forms.Button buttonSE;
        private System.Windows.Forms.Button buttonBGM;
        private System.Windows.Forms.Button buttonBGMStop;
        private System.Windows.Forms.Button buttonCharaname;
        private System.Windows.Forms.Button NoWriteLine;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button Shop;
        private System.Windows.Forms.Button StatusChange;
        private System.Windows.Forms.Button PlaceMove;
        private System.Windows.Forms.Button CallEvent;
        private System.Windows.Forms.Button Battle;
        private System.Windows.Forms.Button Camp;
        private System.Windows.Forms.Button SetLabel;
        private System.Windows.Forms.Button GotoLabel;
        private System.Windows.Forms.Button Break;
        private System.Windows.Forms.Button EventDelete;
        private System.Windows.Forms.Button buttonWait;
        private System.Windows.Forms.Button buttonCharaBack;
        private System.Windows.Forms.Button buttonCharaFront;
        private System.Windows.Forms.Button GameMethod;
        private System.Windows.Forms.Button InputText;
        private System.Windows.Forms.Button NumInput;
    }
}