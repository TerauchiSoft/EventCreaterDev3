﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventCreater
{
    /// <summary>
    /// 命令解析→開くフォーム選択クラス
    /// </summary>
    public static class InstructionAnalysis
    {
        /// <summary>
        /// 前回の引数
        /// </summary>
        public static int[] BefArgs;

        /// <summary>
        /// 前回の文字列引数
        /// </summary>
        public static string BefStrs;

        // ここは3D_rpg向けにコピー
        public static readonly string[] Words = { "playcond", "eventtag", "flag", "val", "menm", "position", "FadeoutBGM", "If", "Else", "Endif", "FullCare", "CharaDelete", "CharaView",
        "Choice", "Branch", "EndChoice", "Effect", "Exp", "Flag", "ScreenFlash", "HPMP", "Item", "LV", "Member", "Money",
        "Note", "ScreenShake", "Skill", "Text", "Variable", "ScreenTone" ,"PlayBGM", "PlaySE", "SubT", "SubN",
        "CharaName", "NoWriteLine", "Battle", "Call", "Camp", "Break", "LabelJump", "Label", "ValueEntry", "MovePlace", "Shop", "Condition", "Delete", "Wait",
        "CharaFront", "CharaBack", "InputText", "GameMethod"};

        /// <summary>
        /// 命令IDサーチ 返り値にID
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static int SeachWord(string str)
        {
            int idxof = 0;
            int instid = 0;
            for (int i = 0; i < Words.Length; i++)
            {
                idxof = str.IndexOf(Words[i]);
                if (idxof == 0)
                {
                    instid = i;
                    return instid;
                }
            }
            return -1;
        }

        /// <summary>
        /// 命令IDサーチ 返り値に文字列
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string SeachWordAsString(string str)
        {
            str = CollectionTool.RemoveChar(str, '\t');

            int idxof = 0;
            int instid = 0;
            for (int i = 0; i < Words.Length; i++)
            {
                idxof = str.IndexOf(Words[i]);
                if (idxof == 0)
                {
                    instid = i;
                    return Words[instid];
                }
            }
            return null;
        }

        /// <summary>
        /// 文字列引数の取得
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string GetStrArg(ref string str)
        {
            if (str.Length < 2) return null;

            // '\"' '\"' ',' delete
            int a = str.IndexOf('\"', 1);
            // 文字列引数
            string strone = "";
            if (a != -1)
            {
                strone = str.Substring(0, a + 1);
                str = str.Remove(0, strone.Length);
                // , 消去
                if (str.Length > 0) str = str.Remove(0, 1);
                // \" \" 削除
                strone = strone.Remove(0, 1);
                strone = strone.Remove(strone.Length - 1, 1);
            }
            return strone;
        }

        /// <summary>
        /// int引数配列の取得
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static int[] GetIntArgs(string str)
        {
            // int引数配列の生成
            string[] stargs = str.Split(',');
            int[] Args = new int[stargs.Length];
            for (int i = 0; i < stargs.Length; i++)
                if (stargs[i].Length > 0)
                    Args[i] = int.Parse(stargs[i]);

            return Args;
        }

        /// <summary>
        /// 命令編集フォームを開く。
        /// 返り値に命令名。isNotOpen = 1でフォームを開かない。
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string OpenEditForm(string str, int isNotOpen = 0)
        {
            // 先頭の'\t'Remove
            str = CollectionTool.RemoveChar(str, '\t');
            // 後ろの'　'Remove
            str = CollectionTool.RemoveChar(str, '　');
            if (str.Length == 0) return null;


            int instid = SeachWord(str);
            if (instid == -1) return null;

            // ()のない命令はここでリターン
            if (str[str.Length - 1] == ':')
                return Words[instid];

            // "instruct(", ")" delete 
            str = str.Remove(0, Words[instid].Length + 1);
            str = str.Remove(str.Length - 1, 1);

            // 文字列引数
            BefStrs = GetStrArg(ref str);

            // int引数配列の生成
            BefArgs = GetIntArgs(str);
            
            return OpenSelectEditForm(instid, BefStrs, BefArgs, isNotOpen);
        }

        /*
         * "FadeoutBGM", "If", "FullCare", "CharaDelete", "CharaView",
         * "Choice", "Effect", "Exp", "Flag", "ScreenFlash", "HPMP", "Item", "LV", "Member", "Money",
         * "Note", "ScreenShake", "Skill", "Text", "Variable", "ScreenTone" ,"PlayBGM", "PlaySE" };
         */

        static int isText = 0;
        static List<string> TextBuf;
        /// <summary>
        /// 解析した文字列から個々の命令編集フォームを開く
        /// </summary>
        /// <param name="idx"></param>
        /// <param name="strone"></param>
        /// <param name="Args"></param>
        /// <returns></returns>
        private static string OpenSelectEditForm(int idx, string strone, int[] Args, int isNotOpen = 0)
        {
            Form fm = null;
            switch (Words[idx])
            {
                case "Default":
                    break;
                case "FadeoutBGM":
                    fm = new BGMFadeOutForm(null, 1, Args);
                    break;
                case "If":
                    fm = new BranchForm(null, 1, Args);
                    break;
                case "FullCare":
                    fm = new CareForm(null, 1, Args);
                    break;
                case "CharaDelete":
                    fm = new CharaDelForm(null, 1, Args);
                    break;
                case "CharaView":
                    fm = new CharaForm(null, 1, Args);
                    break;
                case "Choice":
                    fm = new ChoiceForm(null, 1, strone, Args);
                    break;
                case "Effect":
                    fm = new EffectForm(null, 1, Args);
                    break;
                case "Exp":
                    fm = new ExpForm(null, 1, Args);
                    break;
                case "Flag":
                    fm = new FlagForm(null, 1, Args);
                    break;
                case "ScreenFlash":
                    fm = new FlashForm(null, 1, Args);
                    break;
                case "HPMP":
                    fm = new HPMPForm(null, 1, Args);
                    break;
                case "Item":
                    fm = new IForm(null, 1, Args);
                    break;
                case "LV":
                    fm = new LevelForm(null, 1, Args);
                    break;
                case "Member":
                    fm = new MemberFormcs(null, 1, Args);
                    break;
                case "Money":
                    fm = new MoneyForm(null, 1, Args);
                    break;
                case "Note":
                    // 連続してNoteを並べたときの衝突防止
                    if (isText == 1)
                        break;

                    isText = 1;
                    TextBuf = new List<string>();
                    TextBuf.Add(strone);
                    int i = EventForm.eventForm.EventContents.SelectedIndex + 1;
                    while(true)
                    {
                        string sinst = OpenEditForm(EventData.evCont.codes[i]);
                        if (sinst != "SubN") break;
                        i++;
                    }
                    fm = new NoteForm(null, 1, TextBuf.ToArray(), Args);
                    isText = 0;
                    break;
                case "ScreenShake":
                    fm = new ShakeForm(null, 1, Args);
                    break;
                case "Skill":
                    fm = new SkillForm(null, 1, Args);
                    break;
                case "Text":
                    // 再帰でSubTを連続して取ってくる。
                    // 連続してTextを並べたときの衝突防止
                    if (isText == 1)
                        break;

                    isText = 1;
                    TextBuf = new List<string>();
                    TextBuf.Add(strone);
                    int ii = EventForm.eventForm.EventContents.SelectedIndex + 1;
                    while (true)
                    {
                        // 再帰
                        string sinst = OpenEditForm(EventData.evCont.codes[ii]);
                        if (sinst != "SubT") break;
                        ii++;
                    }
                    fm = new TextForm(null, 1, TextBuf.ToArray(), Args);
                    isText = 0;
                    break;
                case "Variable":
                    fm = new VForm(null, 1, Args);
                    break;
                case "ScreenTone":
                    fm = new WindowColorForm(null, 1, Args);
                    break;
                case "PlayBGM":
                    fm = new SoundForm(0, null, 1, strone, Args);
                    break;
                case "PlaySE":
                    fm = new SoundForm(1, null, 1, strone, Args);
                    break;
                case "SubT":
                    TextBuf.Add(strone);
                    break;
                case "SubN":
                    TextBuf.Add(strone);
                    break;
                case "CharaName":
                    fm = new CharaName(null, 1, strone, Args);
                    break;
                //case "NoWriteLine": ※単一命令
                //    break;
                case "Battle":
                    fm = new BattleForm(null, 1, strone, Args);
                    break;
                case "Call":
                    fm = new EventCallForm(null, 1, strone);
                    break;
                //case "Camp": ※単一命令
                //    fm = new CampForm(null, 1); ← なくていい
                //    break;
                //case "Break": ※単一命令
                //    fm = new BreakForm(null, 1); ← なくていい
                //    break;
                case "LabelJump":
                    fm = new LabelGotoForm(null, 1, Args);
                    break;
                case "Label":
                    fm = new LabelSetForm(null, 1, Args);
                    break;
                case "ValueEntry":
                    fm = new NumInputForm(null, 1, Args);
                    break;
                case "MovePlace":
                    fm = new PlaceMoveForm(null, 1, Args);
                    break;
                case "Shop":
                    fm = new ShopForm(null, 1, strone, Args);
                    break;
                case "Condition":
                    fm = new StatusChangeForm(null, 1, Args);
                    break;
                //case "Delete": ※単一命令
                //    break; ← なくていい
                case "Wait":
                    fm = new WaitForm(null, 1, Args);
                    break;
                case "CharaFront":
                    fm = new CharaFrontForm(null, 1, Args);
                    break;
                case "CharaBack":
                    fm = new CharaBackForm(null, 1, Args);
                    break;
                //case "InputText": ※単一命令
                //    fm = new CampForm(null, 1); ← なくていい
                //    break;
                case "GameMethod":
                    fm = new GameMethod(null, 1, strone);
                    break;
            }
            if (isNotOpen == 0 && (fm != null && isText == 0)) CloseAct.FormOpenCommon(EventForm.eventForm, fm);
            return Words[idx];
        }

        /// <summary>
        /// イベントリスト表示用の文字列の取得
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string GetViewStrs(string str)
        {
            // 先頭の'\t'Remove
            str = CollectionTool.RemoveChar(str, '\t');
            // 後ろの'　'Remove
            str = CollectionTool.RemoveChar(str, '　');
            if (str.Length <= 2) return null;

            int instid = SeachWord(str);
            if (instid == -1) return null;

            // "instruct(", ")" delete 
            str = str.Remove(0, Words[instid].Length + 1);
            // Endifなどの巻き込み防止
            if (str.Length > 0)
                str = str.Remove(str.Length - 1, 1);

            // 文字列引数
            string strone = GetStrArg(ref str);

            // int引数配列の生成
            int[] Args = GetIntArgs(str);

            return GetSelectStrs(instid, strone, Args);
        }

        /// <summary>
        /// 解析した文字列から個々の表示用文字列取得。
        /// </summary>
        /// <param name="idx"></param>
        /// <param name="strone"></param>
        /// <param name="Args"></param>
        /// <returns></returns>
        private static string GetSelectStrs(int idx, string strone, int[] Args)
        {
            string retstr = "";
            switch (Words[idx])
            {
                default:
                    retstr = Words[idx];
                    break;
                case "FadeoutBGM":
                    retstr = "BGMのフェードアウト";
                    break;
                case "If":
                    retstr = "条件分岐";
                    break;
                case "Else":
                    retstr = "条件以外の場合";
                    break;
                case "Endif":
                    retstr = "条件分岐終了";
                    break;
                case "FullCare":
                    retstr = "全回復";
                    break;
                case "CharaDelete":
                    retstr = "キャラクター消去";
                    break;
                case "CharaView":
                    retstr = "キャラクター表示";
                    break;
                case "Choice":
                    retstr = "選択肢の処理";
                    break;
                case "Branch":
                    retstr = "選択肢";
                    break;
                case "EndChoice":
                    retstr = "選択肢処理の終了";
                    break;
                case "Effect":
                    retstr = "マップエフェクト";
                    break;
                case "Exp":
                    retstr = "経験値の増減";
                    break;
                case "Flag":
                    retstr = "フラグの操作";
                    break;
                case "ScreenFlash":
                    retstr = "画面のフラッシュ";
                    break;
                case "HPMP":
                    retstr = "HP,MPの増減";
                    break;
                case "Item":
                    retstr = "アイテムの増減";
                    break;
                case "LV":
                    retstr = "レベルの増減";
                    break;
                case "Member":
                    retstr = "メンバーの入れ替え";
                    break;
                case "Money":
                    retstr = "所持金の増減";
                    break;
                case "Note":
                    retstr = "//";
                    break;
                case "ScreenShake":
                    retstr = "画面のシェイク";
                    break;
                case "Skill":
                    retstr = "スキルの増減";
                    break;
                case "Text":
                    retstr = "文字列の表示";
                    break;
                case "Variable":
                    retstr = "変数の操作";
                    break;
                case "ScreenTone":
                    retstr = "画面の色調操作";
                    break;
                case "PlayBGM":
                    retstr = "BGMの再生";
                    break;
                case "PlaySE":
                    retstr = "効果音の再生";
                    break;
                case "SubT":
                    retstr = "　　　　　　";
                    break;
                case "SubN":
                    retstr = "//";
                    break;
                case "CharaName":
                    retstr = "キャラ名の表示";
                    break;
                case "NoWriteLine":
                    break;
                case "Battle":
                    retstr = "戦闘の処理";
                    break;
                case "Call":
                    retstr = "他イベントの呼び出し";
                    break;
                case "Camp":
                    retstr = "拠点を開く";
                    break;
                case "Break":
                    retstr = "イベント処理の中断";
                    break;
                case "LabelJump":
                    retstr = "指定ラベルに移動";
                    break;
                case "Label":
                    retstr = "ラベルの設置";
                    break;
                case "ValueEntry":
                    retstr = "数値入力の処理";
                    break;
                case "MovePlace":
                    retstr = "場所移動";
                    break;
                case "Shop":
                    retstr = "お店の処理";
                    break;
                case "Condition":
                    retstr = "状態の変更";
                    break;
                case "Delete":
                    retstr = "イベントの一時削除";
                    break;
                case "Wait":
                    retstr = "ウェイト : ";
                    break;
                case "CharaFront":
                    retstr = "キャラを最前面に移動";
                    break;
                case "CharaBack":
                    retstr = "キャラを再背面に移動";
                    break;
                case "InputText":
                    retstr = "テキストの入力";
                    break;
                case "GameMethod":
                    retstr = "ゲーム内メソッドを呼び出す。";
                    break;
            }
            return retstr;
        }
    }
}
