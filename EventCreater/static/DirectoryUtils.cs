﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventCreater
{
    /// <summary>
    /// ディレクトリ、ファイルに関する動作
    /// </summary>
    public static class DirectoryUtils
    {
        /// <summary>
        /// ディレクトリがない場合に新しくディレクトリを追加する。
        /// 読み書きするときに呼び出しておく。
        /// </summary>
        public static void DirectoryCreate()
        {
            Directory.CreateDirectory(AppPath.path + "/Variables");
            Directory.CreateDirectory(AppPath.path + "/Events");
        }

        /// <summary>
        /// ファイルがない場合新しく作成。
        /// </summary>
        private static FileStream CreateFile(string dirName, string fileName)
        {
            FileStream file = File.Create(AppPath.path + "/" + dirName + "/" + fileName + ".dat");
            return file;
        }

        /// <summary>
        /// ファイルを読み込み、List配列にテキストをコピー
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static List<string> ReadFile(string dirName, string fileName, int numline = -1)
        {
            DirectoryCreate();

            List<string> strs = new List<string>();
            FileStream fs;

            if (File.Exists(AppPath.path + "/" + dirName + "/" + fileName + ".dat"))
                fs = new FileStream(AppPath.path + "/" + dirName + "/" + fileName + ".dat", FileMode.Open, FileAccess.Read);
            else
                fs = CreateFile(dirName, fileName);

            try
            {
                //抜けると同時に削除
                using (StreamReader sr = new StreamReader(fs))
                {
                    //一行ごとに読み込める
                    while (!sr.EndOfStream && numline != 0)
                    {
                        // ファイルから一行読み込む
                        var line = sr.ReadLine();
                        strs.Add(line);
                        numline--;
                    }
                }
            }
            catch
            {
                MessageBox.Show("ファイルが壊れているかもしれません。\n直してから開いてみてください。",
                                "エラー",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            fs.Close();
            return strs;
        }

        /// <summary>
        /// ファイルのセーブ
        /// 何か変更したら呼び出す。
        /// </summary>
        /// <param name="fileName"></param>
        public static void SaveFile(string dirName, string fileName, List<string> strs) {
            File.WriteAllLines(AppPath.path + "/" + dirName + "/" + fileName + ".dat", strs);
            SaveVersion();
        }

        /// <summary>
        /// 任意の名前でファイルのセーブ
        /// セーブダイアログから呼び出す。
        /// </summary>
        /// <param name="fileName"></param>
        public static void SaveFile(string filePath, List<string> strs) {
            File.WriteAllLines(filePath, strs);
            SaveVersion();
        }

        /// <summary>
        /// バージョン情報をセーブ
        /// </summary>
        public static void SaveVersion() {
            string versionPath = AppPath.path + "/VERSION.txt";
            string text = DateTime.Now.ToString();
            File.WriteAllText(versionPath, text);
        }
    }
}
