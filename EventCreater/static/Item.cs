﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventCreater
{
    public static class Item
    {
        /// <summary>
        /// ファイル名
        /// </summary>
        public static string[] fname = { "ItemTablev2", "SkillTable" };

        /// <summary>
        /// アイテムの名前
        /// </summary>
        public static List<string> itemName = new List<string>();

        /// <summary>
        /// アイテムの初期最大数(ファイルより読み込み)
        /// </summary>
        private const int InitNumItem = 20;
        
        /// <summary>
        /// <summary>
        /// アイテムの最大数(ファイルより読み込み)
        /// </summary>
        public static int NumItem;

        /// <summary>
        /// 初期フォーマットテキスト
        /// </summary>
        private static string[] initStrTemp =
            {",,0 ~ 255,0 1 2(Anytime BattleOnly MapOnly),0 1 2 3(Recover StatusChange Attack Event),0 1 2 3 4 5 6 7(無 炎 氷 地 雷 風 光 闇 ),0 ~ 3,string[](区切りは\r(Alt+Enter)で区切る。*Usr *Enmで名前),,対象側:enm_str 使用者側:ply_str 等 文字の後ろには1スペース開ける,"
        ,"アイテムID,アイテムの名前,アイテムの効果量,アイテムの種類,アイテムの効果,アイテムの属性,ターゲット(Player Players Enemy Enemys),使用時のアイテムメッセージ,エフェクトのファイル名,スキルの計算式(スキルによって固有の値) ply_amo は効果量,説明文"};

        /// <summary>
        /// ファイルがない場合新しく作成。
        /// </summary>
        private static void CreateitemFile(string fileName)
        {
            FileStream fs = File.Create(AppPath.path + "/Items/" + fileName + ".csv");
            StreamWriter sw = new StreamWriter(fs);
            sw.WriteLine(initStrTemp[0]);
            sw.WriteLine(initStrTemp[1]);

            for (int i = 0; i < InitNumItem; i++)
            {
                sw.WriteLine((i + 1).ToString("D4") + ": ");
            }
            sw.Close();
            fs.Close();
        }
        

        /// <summary>
        /// ファイルを読み込む。アイテム、フラグファイルの名前を指定する。
        /// </summary>
        /// <param name="fileName"></param>
        public static string[] ReaditemFile(string fileName)
        {

            if (!File.Exists(AppPath.path + "/Items/" + fileName + ".csv"))
            {
                // CreateitemFile(fileName);
                MessageBox.Show(AppPath.path + "/items/" + fileName + ".csvがありません。",
                                "エラー",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                return null;
            }
            FileStream fs = new FileStream(AppPath.path + "/Items/" + fileName + ".csv", FileMode.Open, FileAccess.Read);
            try
            {
                //抜けると同時に削除
                using (StreamReader sr = new StreamReader(fs))
                {
                    sr.ReadLine();
                    sr.ReadLine();

                    itemName = new List<string>();

                    int i = 0;
                    //一行ごとに読み込める
                    while (!sr.EndOfStream)
                    {
                        // ファイルから一行読み込む
                        var line = sr.ReadLine();

                        // データを分ける
                        var ar = line.Split(',');

                        itemName.Add(ar[1]);
                        i++;
                    }
                    NumItem = i;
                }
            }
            catch
            {
                MessageBox.Show("アイテム、フラグファイルのフォーマットが壊れているかもしれません。\n直してから開いてみてください。",
                                "エラー",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            fs.Close();
            return itemName.ToArray();
        }

        /// <summary>
        /// ファイルのセーブ
        /// 何か変更したら呼び出す。
        /// </summary>
        /// <param name="fileName"></param>
        public static void SaveitemFile(string fileName)
        {
            List<string> st = new List<string>();
            st.Add(initStrTemp[0]);
            st.Add(initStrTemp[1]);
            st = SetItemString(st);

            FileStream fs = File.Create(AppPath.path + "/ItemTables/" + fileName + ".csv");
            StreamWriter sw = new StreamWriter(fs);
            for (int i = 0; i < st.Count; i++)
            {
                sw.WriteLine(st[i]);
            }
            sw.Close();
            fs.Close();

            SaveVersion();
        }

        /// <summary>
        /// アイテム変数を扱うときは直前に呼び出す。
        /// </summary>
        public static void ItemListLoad()
        {
            ReaditemFile(fname[0]);
        }
        
        /// <summary>
        /// スキル変数を扱うときは直前に呼び出す。
        /// </summary>
        public static void SkillListLoad()
        {
            ReaditemFile(fname[0]);
        }

        /// <summary>
        /// アイテムのインデックスを取得
        /// </summary>
        /// <param name="lbl"></param>
        /// <returns></returns>
        public static int GetitemIndex(Label lbl)
        {
            return int.Parse(lbl.Text.ToString().Substring(0, 4)) - 1;
        }

        /// <summary>
        /// アイテムをList<string>に移す。
        /// これはこのままファイルに出力可能。
        /// </summary>
        /// <param name="st"></param>
        /// <returns></returns>
        public static List<string> SetItemString(List<string> st)
        {
            for (int i = 0; i < NumItem; i++)
            {
                st.Add((i + 1).ToString("D4") + ":" + itemName[i]);
            }
            return st;
        }

        /// <summary>
        /// アイテムページリストを作成。
        /// </summary>
        /// <param name="st"></param>
        /// <returns></returns>
        public static List<string> SetItemPageString(int numofpage)
        {
            List<string> st = new List<string>();
            float numpage = (float)NumItem / numofpage;
            int n = (int)Math.Ceiling(numpage);
            for (int i = 0; i < n; i++)
            {
                string stmin = (numofpage * i + 1).ToString("D4");
                string stmax = (numofpage * (n - 1) > numofpage * i) ?
                    (numofpage * (1 + i)).ToString("D4") : 
                    (numofpage * (NumItem / numofpage) + (NumItem % numofpage)).ToString("D4");
                st.Add(stmin + "-" + stmax);
            }
            return st;
        }

        /// <summary>
        /// "{D4}" + アイテム名 取得
        /// </summary>
        /// <returns></returns>
        public static string GetItemLabelText(int n)
        {
            return (1 + n).ToString("D4") + ":" + Item.itemName[n];
        }


        /// <summary>
        /// アイテム変数のインデックスを取得
        /// </summary>
        /// <param name="lbl"></param>
        /// <returns></returns>
        public static int GetVariableIndex(Label lbl)
        {
            return int.Parse(lbl.Text.ToString().Substring(0, 4)) - 1;
        }

        /// <summary>
        /// アイテム変数のインデックスを取得
        /// </summary>
        /// <param name="lbl"></param>
        /// <returns></returns>
        public static int GetVariableIndex(string str)
        {
            return int.Parse(str.ToString().Substring(0, 4)) - 1;
        }

        /// <summary>
        /// アイテムの最大数変更
        /// </summary>
        public static void NumItemChange(int num, string st)
        {
            NumItem = num;
            if (num > itemName.Count)
            {
                while (num > itemName.Count)
                    itemName.Add(" ");
            }
            else
                itemName.RemoveRange(num, itemName.Count - num);

            SaveitemFile(st);
        }

        /// <summary>
        /// コンボボックスにアイテムロード
        /// </summary>
        /// <param name="comboBoxitem"></param>
        public static void ItemLoadAndSet(ComboBox comboBoxitem)
        {
            comboBoxitem.Items.Clear();
            comboBoxitem.Items.AddRange(ReaditemFile(fname[0]));
            comboBoxitem.Items.AddRange(itemName.ToArray());
            comboBoxitem.SelectedIndex = 0;
        }
        
        /// <summary>
        /// コンボボックスにアイテムロード
        /// </summary>
        /// <param name="comboBoxitem"></param>
        public static void SkillLoadAndSet(ComboBox comboBoxitem)
        {
            comboBoxitem.Items.Clear();
            comboBoxitem.Items.AddRange(ReaditemFile(fname[1]));
            comboBoxitem.SelectedIndex = 0;
        }

        /// <summary>
        /// バージョン情報をセーブ
        /// </summary>
        public static void SaveVersion() {
            string versionPath = AppPath.path + "/VERSION.txt";
            string text = DateTime.Now.ToString();
            File.WriteAllText(versionPath, text);
        }
    }
}

